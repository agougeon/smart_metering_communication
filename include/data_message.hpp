#pragma once
#include <string>
#include "phase.hpp"
#include "heaters_status.hpp"

class DataMessage {
public:
    double value_kw;
    double i_a_ka;
    double i_b_ka;
    double i_c_ka;
    double vm_a_pu;
    double vm_b_pu;
    double vm_c_pu;
    std::string sender_name;   
    Phase phase;
    HeatersStatus heaters_status;

    DataMessage(double value_kw_,
                std::string sender_name_,
                Phase phase_,
                HeatersStatus heaters_status_);

    DataMessage(double i_a_ka_,
                double i_b_ka_,
                double i_c_ka_,
                double vm_a_pu_,
                double vm_b_pu_,
                double vm_c_pu_,
                std::string sender_name_,
                Phase phase_ = Phase::ALL);
};
