#pragma once
#include <string>
#include <list>
#include "simgrid/s4u.hpp"
#include "heaters_status.hpp"

class SmartMeterData {
public:
    /// attributes defined at construction
    std::string name;
    simgrid::s4u::Mailbox* mailbox_controller;


    /// attributes modified at runtime
    int shutdown_count = 0;
    double load_kw;
    HeatersStatus heaters_status = HeatersStatus::ON;

    SmartMeterData(std::string name_);

    void update(double new_load_kw_, HeatersStatus new_heater_status);
};
