#pragma once
#include <string>
#include <map>

class Option {
	private:
	std::string name;
	std::string value;
	std::string description;
	bool mandatory;
	bool manually_set;

	public:
	Option(std::string name, std::string value, std::string description, bool mandatory, bool manually_set=false) :
		name(name), value(value), description(description), mandatory(mandatory), manually_set(manually_set) {};
	std::string get_name() {return name;}
	void set_value(std::string v) {value = v; manually_set=true;}
	std::string get_value() {return value;}
	bool is_mandatory() {return mandatory;}
	bool is_manually_set() {return manually_set;}
};

class Argparse {
	public:
	static std::map<std::string, Option*> opts;

	static void set(std::string name, std::string value);
	static void check_mandatory();

	static void add_option(std::string name, bool mandatory=false, std::string default_value="", std::string description="no description given");
	static void parse(int argc, char** argv);
	static std::string get(std::string name);
};
