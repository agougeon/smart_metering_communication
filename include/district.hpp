#pragma once
#include "simgrid/s4u.hpp"
#include "config.hpp"
#include "master.hpp"
#include "substation.hpp"
#include "smartmeter.hpp"
#include "phase.hpp"

class District {
public:
    /// pre-defined attributes
    int start_time = 63000;
    int nb_total_houses = 55;
    std::vector<std::pair<std::string, Phase>> fmu_input = {
        std::pair<std::string, Phase>("asymmetric_load/LOAD1/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD2/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD3/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD4/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD5/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD6/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD7/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD8/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD9/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD10/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD11/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD12/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD13/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD14/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD15/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD16/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD17/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD18/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD19/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD20/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD21/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD22/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD23/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD24/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD25/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD26/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD27/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD28/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD29/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD30/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD31/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD32/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD33/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD34/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD35/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD36/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD37/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD38/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD39/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD40/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD41/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD42/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD43/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD44/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD45/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD46/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD47/p_c_mw", Phase::C),
        std::pair<std::string, Phase>("asymmetric_load/LOAD48/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD49/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD50/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD51/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD52/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD53/p_b_mw", Phase::B),
        std::pair<std::string, Phase>("asymmetric_load/LOAD54/p_a_mw", Phase::A),
        std::pair<std::string, Phase>("asymmetric_load/LOAD55/p_a_mw", Phase::A)};

    /// attributes defined at construction
    int id;
    Master* master;
    Substation* substation;
    std::vector<SmartMeter*> smartmeters;

    /// return the name of the next smartmeter in its ring
    SmartMeter* get_next_smartmeter_in_phase(Phase phase, int index);

    /// set next mailbox and smartmeter to send token for each smart meter in the ring
    /// useful in decentralized management
    void set_nexts();

    /// build a district including smartmeters, substation and master
    /// id: number associated to this district in the platform
    District(int id, Config config);
};
