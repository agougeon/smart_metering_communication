#pragma once
#include "simgrid/s4u.hpp"

class Substation {
    /// pre-definied attributes
    std::string fmu_name = "pandapower";

    /// attributes defined at construction
    int start_time_s;
    int sampling_period_s;
    int message_size_bytes;
    std::string log_dir;
    simgrid::s4u::Host* host;
    simgrid::s4u::Mailbox* mailbox_data_master;

    /// create the various actors for this smart meter
    void create_actors();

    /// actor sending regularly data message to the master
    static void sampler(Substation* substation);

    /// actor logging every sec the current in each phase
    static void log_sampler(Substation* substation);

public:
    Substation(int start_time_s_,
               int sampling_period_s_,
               int message_size_bytes_,
               std::string log_dir_,
               simgrid::s4u::Host* host_,
               simgrid::s4u::Mailbox* mailbox_master_);
};
