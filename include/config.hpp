#pragma once
#include <string>
#include "management.hpp"

class Config{
public:
    int nb_sheddable_houses;
    int message_size_bytes;
    int sampling_period_s;
    int max_shutdown_duration_s;
    double upper_current_threshold_ka;
    std::string fmu_path;
	std::string platform_path;
	std::string flexible_loads_dir;
	std::string non_flexible_loads_dir;
	std::string log_dir;
    Management management;

	explicit Config(int argc, char** argv);

    void log_config();

    void create_log_file_shutdowns();
};
