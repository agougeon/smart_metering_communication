#pragma once
#include "simgrid/s4u.hpp"
#include "management.hpp"
#include "data_message.hpp"
#include "smartmeter_data.hpp"
#include "phase.hpp"

class Master{
    /// pre-defined attributes
    const double no_heaters_per_house = 3;
    const double heater_load_kw = 2;
    const double single_phase_voltage_v = 240;

    /// attributes defined at construction
    const int id;
    const int control_message_size_bytes = 1e3;
    const double upper_threshold_ka;
    std::string log_dir;
    simgrid::s4u::Host* host;
    const Management management;
    std::unordered_map<Phase, simgrid::s4u::Mailbox*> phase_control_mailboxes ={
        {Phase::A, nullptr},
        {Phase::B, nullptr},
        {Phase::C, nullptr},
    };
    simgrid::s4u::Mailbox* mailbox_control_phase_b;
    simgrid::s4u::Mailbox* mailbox_control_phase_c;
    simgrid::s4u::Mailbox* mailbox_data_smartmeter;
    simgrid::s4u::Mailbox* mailbox_data_substation;
    simgrid::s4u::Mailbox* mailbox_status;
    simgrid::s4u::Mailbox* mailbox_token;
    std::unordered_map<Phase, std::string> next_mailbox_per_ring; // used when management decentralized

    /// attributes modified at run time
    int control_messages_sent = 0;
    int data_message_substation_received = 0;
    int data_message_smartmeter_received = 0;
    int status_received = 0;
    int token_master_sent = 0;
    int token_smartmeter_sent = 0;
    std::unordered_map<Phase, std::vector<SmartMeterData*>> phase_smartmeters_data; /// smartmeters sorted by phase
    std::unordered_map<std::string, SmartMeterData*> name_smartmeters_data; /// smartmeters sorted by name
    std::unordered_map<Phase, bool> phase_sending_in_progress = {
        {Phase::A, false},
        {Phase::B, false},
        {Phase::C, false},
    };
    std::unordered_map<Phase, bool> token_sender_lock = {
        {Phase::A, false},
        {Phase::B, false},
        {Phase::C, false},
    };
    std::unordered_map<Phase, int> phases_cycles = {
        {Phase::A,1},
        {Phase::B,1},
        {Phase::C,1},
    };
    std::unordered_map<Phase, double> phases_lower_thresholds_ka = {
        {Phase::A,0},
        {Phase::B,0},
        {Phase::C,0},
    };

    /// create a new entry of data for a smart meter
    void add_smartmeter_data(DataMessage* message);

    /// send a shutdown command to a smartmeter
    void send_shutdown_command(SmartMeterData* sm_data);

    /// start or continue cascado_cyclic process for a phase (centralized management)
    void cascado_cyclic(int houses_to_shutdown, Phase phase);

    /// start or continue cascado_cyclic process for a phase (decentralized management)
    void cascado_cyclic_decentralized(int houses_to_shutdown, Phase phase);

    /// actor waiting to receive data messages from the substation
    static void data_handler_substation(Master* master);

    /// actor waiting to receive data messages from smartmeters
    static void data_handler_smartmeter(Master* master);

    /// actor waiting to receive token messages (decentralized)
    static void status_handler(Master* master);

    /// actor waiting to receive token messages (decentralized)
    static void token_handler(Master* master);

    /// create the various actors for this smart meter
    void create_actors();

    /// create mailboxes to send or receive messages
    void create_mailboxes();

public:
    /// return name of the host
    std::string get_name();

    /// log message count
    void log_messages_count();

    /// set mailboxes of the firsts smartmeters of each ring (one per phase)
    /// useful for decentralized management
    void set_first_mailbox_per_ring(std::string a, std::string b, std::string c);

    explicit Master(int id_,
                        double upper_threshold_ka_,
                        std::string log_dir_,
                        simgrid::s4u::Host* host_,
                        Management management_);
};
