#pragma once
#include <string>
#include "simgrid/s4u.hpp"
#include "phase.hpp"
#include "heaters_status.hpp"
#include "management.hpp"
#include <list>

class SmartMeter {
    /// pre-definied attributes
    const std::string fmu_name = "pandapower";
    const int sliding_window_size_s = 300;
    const int control_message_size_bytes = 1e3;
    const int status_message_size_bytes = 1e2;

    /// attributes defined at construction
    const int start_time_s;
    const int sampling_period_s;
    const int max_shutdown_duration_s;
    const int data_message_size_bytes;
    const bool sheddable;
    const std::string fmu_input_p;
    std::string fmu_input_q;
    const std::string log_dir;
    std::string master_name;
    std::vector<std::pair<int, double>> time_s_flexible_load_kw;
    std::vector<std::pair<int, double>> time_s_non_flexible_load_kw;
    simgrid::s4u::Host* host;
    simgrid::s4u::Mailbox* mailbox_controller;
    simgrid::s4u::Mailbox* mailbox_data_master;
    simgrid::s4u::Mailbox* mailbox_status_master;
    simgrid::s4u::Mailbox* mailbox_token;
    simgrid::s4u::Mailbox* mailbox_token_next;
    simgrid::s4u::Mailbox* mailbox_token_master;
    SmartMeter* next_smartmeter;
    const Phase phase;
    const Management management;

    /// attributes modified at runtime
    int nb_shutdown = 0;
    HeatersStatus heaters_status = HeatersStatus::ON;
    double flexible_load_kw = 0;
    double non_flexible_load_kw = 0;
    double load_kw = 0;
    double shutdown_date;
    std::list<double> sliding_window_loads_kw = {0};
    simgrid::s4u::ActorPtr shutdown_timer_actor;

    /// log shutdown duration
    void log_shutdown();

    /// return the average load of the smart meter
    double get_average_load_kw();

    /// load flexible load file in its vector
    void create_vector_flexible_load(std::string path);

    /// load non flexible load file in its vector
    void create_vector_non_flexible_load(std::string path);

    /// update the load in attribute and in the fmu
    void update_load();

    /// update the average load value of the smartmeter
    static void sliding_window_updater(SmartMeter* smart_meter);

    /// actor upating flexible load attribute regularly
    static void flexible_load_updater(SmartMeter* smart_meter);

    /// actor upating non flexible load attribute regularly
    static void non_flexible_load_updater(SmartMeter* smart_meter);

    /// actor ensuring that a shutdown do not exceed a max duration
    static void shutdown_timer(SmartMeter* smart_meter);

    /// turn on heaters and call update_load
    void turn_on_heaters();

    /// actor waiting to receive control messages (centralized)
    static void controller(SmartMeter* smart_meter);

    /// actor waiting to receive control messages (decentralized)
    static void controller_decentralized(SmartMeter* smart_meter);

    /// send consumption and status info to master
    void send_data_to_master();

    /// send status to master
    void send_status_to_master();

    /// actor sending regularly data message to the master
    static void sampler(SmartMeter* smart_meter);

    /// create the various actors for this smart meter
    void create_actors();

    /// create mailboxes to send or receive messages
    void create_mailboxes();

public:
    /// return the name of the smartmeter
    std::string get_name();

    /// return the phase to which the smartmeter is connected
    Phase get_phase();

    /// return true if the smartmeter is sheddable
    bool is_sheddable();

    /// return the input port of the power for this smartmeter inside the fmu^
    std::string get_fmu_input_p();

    /// set the next mailbox this smartmeter will sends token to
    /// useful in decentralized management
    void set_next_mailbox(simgrid::s4u::Mailbox* mailbox);

    /// store a pointer to the next smartmeter in the ring (decentralized only)
    void set_next_smartmeter(SmartMeter* next_smartmeter);

    /// get a pointer on the smartmeter following this one on the ring (decentralized only)
    SmartMeter* get_next_smartmeter();

    /// turn off heaters and call update_load
    void turn_off_heaters();

    explicit SmartMeter(int start_time_s_,
                        int sampling_period_s_,
                        int max_shutdown_duration_s_,
                        int data_message_size_bytes_,
                        bool sheddable_,
                        std::string flexible_loads_dir,
                        std::string non_flexible_loads_dir,
                        std::string fmu_input_,
                        std::string log_dir_,
                        std::string master_name_,
                        simgrid::s4u::Host* host_,
                        Phase phase_,
                        Management management_);
};
