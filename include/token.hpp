#pragma once
#include "phase.hpp"
#include "simgrid/s4u.hpp"

class Token {
public:
    Phase phase;
    int houses_to_shutdown;
    std::string next_mailbox;

    Token(Phase phase_, int houses_to_shutdown_, std::string next_mailbox_ = "undefined");
    Token(const Token* t);
};
