#include "data_message.hpp"
#include "phase.hpp"

DataMessage::DataMessage(double value_kw_,
                         std::string sender_name_,
                         Phase phase_,
                         HeatersStatus heaters_status_) :
    value_kw(value_kw_),
    sender_name(sender_name_),
    phase(phase_),
    heaters_status(heaters_status_)
{};

DataMessage::DataMessage(double i_a_ka_,
                         double i_b_ka_,
                         double i_c_ka_,
                         double vm_a_pu_,
                         double vm_b_pu_,
                         double vm_c_pu_,
                         std::string sender_name_,
                         Phase phase_) :
    i_a_ka(i_a_ka_),
    i_b_ka(i_b_ka_),
    i_c_ka(i_c_ka_),
    vm_a_pu(vm_a_pu_),
    vm_b_pu(vm_b_pu_),
    vm_c_pu(vm_c_pu_),
    sender_name(sender_name_),
    phase(phase_) {};
