#include "token.hpp"

Token::Token(Phase phase_,
             int houses_to_shutdown_,
             std::string next_mailbox_) :
    phase(phase_),
    houses_to_shutdown(houses_to_shutdown_),
    next_mailbox(next_mailbox_)
{};

Token::Token(const Token* t) {
    this->phase = t->phase;
    this->houses_to_shutdown = t->houses_to_shutdown;
    this->next_mailbox = t->next_mailbox;
}
