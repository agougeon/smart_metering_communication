#include <fstream>
#include "argparse.h"
#include "config.hpp"



Config::Config(int argc, char** argv) {
    Argparse::add_option("-nb_sheddable_houses", false, "30");
    Argparse::add_option("-sampling_period_s", false, "20");
    Argparse::add_option("-max_shutdown_duration_s", false, "60");
    Argparse::add_option("-message_size_bytes", false, "1000");
    Argparse::add_option("-upper_current_threshold_ka", false, "0.50");
    Argparse::add_option("-fmu_path", false, "../resources/pandapower_fmu/unbalanced/pandapower.fmu");
//    Argparse::add_option("-platform_path", false, "../resources/platforms/wired_10ms.xml");
    Argparse::add_option("-platform_path", false, "../resources/platforms/wireless_10ms.xml");
	Argparse::add_option("-flexible_loads_dir", false, "../resources/input/flexible_loads");
	Argparse::add_option("-non_flexible_loads_dir", false, "../resources/input/non_flexible_loads");
	Argparse::add_option("-log_dir", false, "../resources/logs");
    Argparse::add_option("-management", false, "1");

    Argparse::parse(argc, argv);

    nb_sheddable_houses = std::stoi(Argparse::get("-nb_sheddable_houses"));
	management = Argparse::get("-management") == "1" ? Management::CENTRALIZED : Management::DECENTRALIZED;
    message_size_bytes = std::stod(Argparse::get("-message_size_bytes"));
    max_shutdown_duration_s = std::stoi(Argparse::get("-max_shutdown_duration_s"));
    sampling_period_s = std::stoi(Argparse::get("-sampling_period_s"));
    upper_current_threshold_ka = std::stod(Argparse::get("-upper_current_threshold_ka"));
    fmu_path = Argparse::get("-fmu_path");
	platform_path = Argparse::get("-platform_path");	
    flexible_loads_dir = Argparse::get("-flexible_loads_dir");
    non_flexible_loads_dir = Argparse::get("-non_flexible_loads_dir");
	log_dir = Argparse::get("-log_dir");

    log_config();
    create_log_file_shutdowns();
}

void Config::log_config() {
    if (system(("mkdir -p " + log_dir).c_str()) != 0)
        throw("Cannot create log dir");

    std::ofstream config_file(log_dir + "/config.csv");
    config_file << "Management,Sheddable Houses,Upper Threshold (kA),Message Size (Bytes),Shutdown Duration (s),Sampling Period (s),Platform" << std::endl
                << (management == Management::CENTRALIZED ? "centralized" : "decentralized") << ","
                << nb_sheddable_houses << ","
                << upper_current_threshold_ka << ","
                << message_size_bytes << ","
                << max_shutdown_duration_s << ","
                << sampling_period_s << ","
                << platform_path.substr(platform_path.find_last_of("/") + 1);
    config_file.close();
}

void Config::create_log_file_shutdowns() {
    if (system(("mkdir -p " + log_dir).c_str()) != 0)
        throw("Cannot create log dir");

    std::ofstream shutdowns(log_dir + "/shutdowns.csv");
    shutdowns << "Name,Phase,Date (s),Duration (s)" << std::endl;
    shutdowns.close();

    /// create or clear log file
    std::ofstream master_log(log_dir + "/master_log.txt");
    master_log.close();
}
