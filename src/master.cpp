#include <math.h>
#include <numeric>
#include <fstream>
#include <iostream>
#include "master.hpp"
#include "control_message.hpp"
#include "data_message.hpp"
#include "phase.hpp"
#include "smartmeter_data.hpp"
#include "token.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(master, "Messages specific for this s4u example");

Master::Master(int id_,
                       double upper_threshold_ka_,
                       std::string log_dir_,
                       simgrid::s4u::Host* host_,
                       Management management_) :
    id(id_),
    upper_threshold_ka(upper_threshold_ka_),
    log_dir(log_dir_),
    host(host_),
    management(management_)
{
    create_mailboxes();
    create_actors();
    XBT_DEBUG("Master %d: master created on host: %s.", id, host->get_cname());
};

void Master::add_smartmeter_data(DataMessage* message) {
    phase_smartmeters_data[message->phase].push_back(new SmartMeterData(message->sender_name));
    phase_smartmeters_data[message->phase].back()->update(message->value_kw, message->heaters_status);
    name_smartmeters_data[message->sender_name] = phase_smartmeters_data[message->phase].back();
}

void Master::send_shutdown_command(SmartMeterData* sm_data) {
    xbt_assert(sm_data->heaters_status != HeatersStatus::OFF, "cannot send shutdown command, its already OFF");
    static ControlMessage control_message = ControlMessage::SHUTDOWN;
    //simgrid::s4u::CommPtr com = mailbox_controller->put_async(&control_message, message_size_bytes); parallel send
    //com.detach();
    sm_data->mailbox_controller->put(&control_message, control_message_size_bytes); // sequential send
    sm_data->heaters_status = HeatersStatus::OFF_TO_CONFIRM;
}

void Master::cascado_cyclic(int houses_to_shutdown, Phase phase) {
    std::ofstream master_log(log_dir + "/master_log.txt",  std::ios_base::app);
    master_log << "--------------------" << std::endl;
    master_log << "[" << simgrid::s4u::Engine::get_clock() << "]" << " cascado cyclic process start" <<std::endl;
    master_log << "[" << simgrid::s4u::Engine::get_clock() << "]" << " phase " << (phase == Phase::A ? "A" : phase == Phase::B ? "B" : "C") << std::endl;
    master_log << "[" << simgrid::s4u::Engine::get_clock() << "]" << " houses to shed " << houses_to_shutdown << std::endl;
    master_log << "[" << simgrid::s4u::Engine::get_clock() << "]" << " sheddable houses on this phase " << phase_smartmeters_data[phase].size() << std::endl;
    if (phase_sending_in_progress[phase]) {
        master_log << "sending already in progress on that phase" <<std::endl;
        return;
    }
    phase_sending_in_progress[phase] = true; /// lock sending

    /// sort smartmeters in that phase by decreasing load
    std::sort(phase_smartmeters_data[phase].begin(),
              phase_smartmeters_data[phase].end(),
              [](SmartMeterData* a, SmartMeterData* b){return a->load_kw > b->load_kw;});

    int phase_cycle = phases_cycles[phase];
    master_log << "[" << simgrid::s4u::Engine::get_clock() << "]" << " phase cycle: " << phase_cycle << std::endl;
    bool update_cycle = true;

    auto iterator = phase_smartmeters_data[phase].begin();
    while (houses_to_shutdown > 0 and iterator != phase_smartmeters_data[phase].end()) {
        SmartMeterData* sm_data = (*iterator);
        master_log << "- " << sm_data->name << std::endl;
        iterator++;

        HeatersStatus house_status = sm_data->heaters_status;
        master_log << "status: "
                       << (sm_data->heaters_status == HeatersStatus::ON ? "ON" :
                           sm_data->heaters_status == HeatersStatus::OFF ? "OFF" :
                           sm_data->heaters_status == HeatersStatus::ON_TO_CONFIRM ? "ON?" :
                           "OFF?") << std::endl;
        int house_shutdown_count = sm_data->shutdown_count;
        master_log << "shutdown count: " << house_shutdown_count << std::endl;

        update_cycle &= house_shutdown_count == phase_cycle;
        if (house_shutdown_count == phase_cycle) { /// house has already been shed
            master_log << "ready for next cycle" << std::endl;
            if (update_cycle && iterator == phase_smartmeters_data[phase].end()) { /// all previous house has been shed and its the end
                phases_cycles[phase]++;
                phase_cycle = phases_cycles[phase];
                master_log << "[" << simgrid::s4u::Engine::get_clock() << "]"
                               << " starting new cycle (" << phases_cycles[phase] << ")" << std::endl; /// increase cycle
                iterator = phase_smartmeters_data[phase].begin(); /// start from the beginning
            }
        }
        else if (house_status == HeatersStatus::ON) { /// house ON
            master_log << "shutting down " << std::endl;
            send_shutdown_command(sm_data); /// shed house
            control_messages_sent++;
            houses_to_shutdown--;

            }
    }
    master_log << "--------------------" << std::endl;
    master_log.close();
    phase_sending_in_progress[phase] = false; /// unlock sending
}

void Master::cascado_cyclic_decentralized(int houses_to_shutdown, Phase phase) {
    if (token_sender_lock[phase])
        return;

    XBT_DEBUG("Master %d: cascado_cyclic_decentralized: phase %s overloaded. %d houses must be shed.",
              id, phase == Phase::A ? "A" : phase == Phase::B ? "B" : "C", houses_to_shutdown);
    XBT_DEBUG("Master %d: sending token to next smartmeter in ring: %s", id, next_mailbox_per_ring[phase].c_str());
    simgrid::s4u::CommPtr com = simgrid::s4u::Mailbox::by_name(next_mailbox_per_ring[phase])->put_async(
                new Token(phase,houses_to_shutdown),control_message_size_bytes);
    com.detach();
    token_master_sent++;
    token_smartmeter_sent += houses_to_shutdown;
    token_sender_lock[phase] = true;
}

void Master::data_handler_substation(Master* master) {
    DataMessage* data_message;
    std::unordered_map<Phase, double> i_ka_per_phase;
    std::unordered_map<Phase, double> vm_pu_per_phase;
    Phase phase;
    double i_ka;
    double delta_i_ka;
    int houses_to_shutdown;
    while(true) {
        data_message = master->mailbox_data_substation->get<DataMessage>();
        master->data_message_substation_received++;
        i_ka_per_phase = {
            {Phase::A, data_message->i_a_ka},
            {Phase::B, data_message->i_b_ka},
            {Phase::C, data_message->i_c_ka},
        };
        vm_pu_per_phase = {
            {Phase::A, data_message->vm_a_pu},
            {Phase::B, data_message->vm_b_pu},
            {Phase::C, data_message->vm_c_pu},
        };
        for (auto key_value : i_ka_per_phase) {
            phase = key_value.first;
            i_ka = key_value.second;
            delta_i_ka = i_ka - master->upper_threshold_ka;
            if (delta_i_ka > 0) {
                houses_to_shutdown = std::ceil(delta_i_ka * vm_pu_per_phase[phase] * master->single_phase_voltage_v / master->heater_load_kw);
                if (master->management == Management::CENTRALIZED)
                    master->cascado_cyclic(houses_to_shutdown, phase);
                else
                    master->cascado_cyclic_decentralized(houses_to_shutdown, phase);
            }
        }
    }
}

void Master::data_handler_smartmeter(Master* master) {
    std::ofstream master_log(master->log_dir + "/master_log.txt",  std::ios_base::app);

    DataMessage* data_message;
    while(true) {
        data_message = master->mailbox_data_smartmeter->get<DataMessage>();
        master->data_message_smartmeter_received++;
        master_log <<"[" << simgrid::s4u::Engine::get_clock() << "]" << " data received from " << data_message->sender_name << std::endl;
        if (master->name_smartmeters_data.find(data_message->sender_name) == master->name_smartmeters_data.end())
            master->add_smartmeter_data(data_message);
        else
            master->name_smartmeters_data[data_message->sender_name]->update(data_message->value_kw, data_message->heaters_status);
    }
}

void Master::status_handler(Master* master) {
    std::ofstream master_log(master->log_dir + "/master_log.txt",  std::ios_base::app);

    DataMessage* data_message;
    while(true) {
        data_message = master->mailbox_status->get<DataMessage>();
        master->status_received++;
        master_log <<"[" << simgrid::s4u::Engine::get_clock() << "]" << " status received from " << data_message->sender_name << std::endl;
        if (master->name_smartmeters_data.find(data_message->sender_name) != master->name_smartmeters_data.end())
            master->name_smartmeters_data[data_message->sender_name]->update(data_message->value_kw, data_message->heaters_status);
    }
}

void Master::token_handler(Master* master) {
    Token* token;
    while(true) {
        token = master->mailbox_token->get<Token>();
        if (token->houses_to_shutdown > 0)
            master->token_smartmeter_sent -= token->houses_to_shutdown - 1;
        master->next_mailbox_per_ring[token->phase] = token->next_mailbox;
        master->token_sender_lock[token->phase] = false;
    }
}

void Master::create_actors() {
    typedef void (func)(Master*);
    simgrid::s4u::ActorPtr current_actor;

    std::vector<std::pair<std::string, func*>> control_funcs = {
        std::pair<std::string, func*>("data_handler_substation", &data_handler_substation)
    };
    if (management == Management::CENTRALIZED) {
        XBT_DEBUG("%s: create_actors: centralized management detected. Adding data_handler_smartmeter actor and status_handler actor.",
                  host->get_cname());
        control_funcs.push_back(std::pair<std::string, func*>("data_handler_smartmeter", &data_handler_smartmeter));
        control_funcs.push_back(std::pair<std::string, func*>("status_handler", &status_handler));
    }
    else {
        XBT_DEBUG("%s: create_actors: decentralized management detected. Adding token_handler actor.", host->get_cname());
        control_funcs.push_back(std::pair<std::string, func*>("token_handler", &token_handler));
    }

    for (auto p : control_funcs) {
        current_actor = simgrid::s4u::Actor::create(p.first, host, p.second, this);
        current_actor->daemonize();
    }
}

void Master::create_mailboxes() {
    mailbox_data_substation = simgrid::s4u::Mailbox::by_name(get_name() + "_data_substation");

    if (management == Management::CENTRALIZED) {
        mailbox_data_smartmeter = simgrid::s4u::Mailbox::by_name(get_name() + "_data_smartmeter");
        mailbox_status = simgrid::s4u::Mailbox::by_name(get_name() + "_status");
        phase_control_mailboxes[Phase::A] = simgrid::s4u::Mailbox::by_name(get_name() + "control_phase_a");
        phase_control_mailboxes[Phase::B] = simgrid::s4u::Mailbox::by_name(get_name() + "control_phase_b");
        phase_control_mailboxes[Phase::C] = simgrid::s4u::Mailbox::by_name(get_name() + "control_phase_c");
    }
    else
        mailbox_token = simgrid::s4u::Mailbox::by_name(get_name() + "_token");
}

void Master::set_first_mailbox_per_ring(std::string mailbox_a, std::string mailbox_b, std::string mailbox_c) {
    next_mailbox_per_ring[Phase::A] = mailbox_a;
    next_mailbox_per_ring[Phase::B] = mailbox_b;
    next_mailbox_per_ring[Phase::C] = mailbox_c;
    XBT_DEBUG("Master %d: first mailboxes in rings (phase a, b, c) set to: %s, %s, %s",
              id, mailbox_a.c_str(), mailbox_b.c_str(), mailbox_c.c_str());
}

std::string Master::get_name() {
    return host->get_name();
}

void Master::log_messages_count() {
    std::ofstream file(log_dir + "/messages_count.csv");
    file << "Type,Count" << std::endl;
    file << "Control Sent," << control_messages_sent << std::endl;
    file << "Data Substation Received," << data_message_substation_received << std::endl;
    file << "Data Smartmeter Received," << data_message_smartmeter_received << std::endl;
    file << "Status Received," << status_received << std::endl;
    file << "Token Master Sent," << token_master_sent << std::endl;
    file << "Token Smartmeter Sent," << token_smartmeter_sent << std::endl;
    file.close();
}
