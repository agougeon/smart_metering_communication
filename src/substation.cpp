#include "substation.hpp"
#include "data_message.hpp"
#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <fstream>

XBT_LOG_NEW_DEFAULT_CATEGORY(substation, "Messages specific for this s4u example");

Substation::Substation(int start_time_s_,
                       int sampling_period_s_,
                       int message_size_bytes_,
                       std::string log_dir_,
                       simgrid::s4u::Host *host_,
                       simgrid::s4u::Mailbox* mailbox_data_master_) :
    start_time_s(start_time_s_),
    sampling_period_s(sampling_period_s_),
    message_size_bytes(message_size_bytes_),
    log_dir(log_dir_),
    host(host_),
    mailbox_data_master(mailbox_data_master_)

{
    create_actors();
    XBT_DEBUG("Substation: substation created on host: %s.", host->get_cname());
}

void Substation::sampler(Substation* substation) {
    simgrid::s4u::this_actor::sleep_until(substation->start_time_s);
    while(true) {
        simgrid::s4u::CommPtr com = substation->mailbox_data_master->put_async(
                    new DataMessage(simgrid::fmi::get_real(substation->fmu_name,"res_line_3ph/LINE1_0_0/i_a_ka"),
                                    simgrid::fmi::get_real(substation->fmu_name,"res_line_3ph/LINE1_0_0/i_b_ka"),
                                    simgrid::fmi::get_real(substation->fmu_name,"res_line_3ph/LINE1_0_0/i_c_ka"),
                                    simgrid::fmi::get_real(substation->fmu_name,"res_bus_3ph/bus_ELVTF_15_0_0/vm_a_pu"),
                                    simgrid::fmi::get_real(substation->fmu_name,"res_bus_3ph/bus_ELVTF_15_0_0/vm_b_pu"),
                                    simgrid::fmi::get_real(substation->fmu_name,"res_bus_3ph/bus_ELVTF_15_0_0/vm_c_pu"),
                                    substation->host->get_name()),
                    substation->message_size_bytes);
        com.detach();
        simgrid::s4u::this_actor::sleep_for(substation->sampling_period_s);
    }
}

void Substation::log_sampler(Substation *substation) {
    simgrid::s4u::this_actor::sleep_until(substation->start_time_s);
    std::ofstream substation_sampler(substation->log_dir + "/substation_sampler.csv");
    std::vector<std::string> fmu_outputs = {"i_a_ka", "i_b_ka", "i_c_ka"};
    std::vector<std::string> phases = {"A", "B", "C"};
    std::vector<double> last_measures = {0, 0, 0};
    substation_sampler << "Time (s),Phase,Current (kA)" << std::endl;
    double tmp = 0;
    while(true) {
        for (int i=0; i < (int) fmu_outputs.size(); i++) {
            tmp = simgrid::fmi::get_real(substation->fmu_name, "res_line_3ph/LINE1_0_0/" + fmu_outputs[i]);
            if (tmp != last_measures[i]) {
                substation_sampler << simgrid::s4u::Engine::get_clock() << ","
                                   << phases[i] << ","
                                   << tmp << std::endl;
                last_measures[i] = tmp;
            }
        }
        simgrid::s4u::this_actor::sleep_for(substation->sampling_period_s);
    }
}

void Substation::create_actors() {
    typedef void (func)(Substation*);
    simgrid::s4u::ActorPtr current_actor;

    std::vector<std::pair<std::string, func*>> func_vector = {
        std::pair<std::string, func*>("sampler", &sampler),
        std::pair<std::string, func*>("log_sampler", &log_sampler),
    };

    for (auto p : func_vector) {
        current_actor = simgrid::s4u::Actor::create(p.first, host, p.second, this);
        current_actor->daemonize();
    }
}
