#include "smartmeter_data.hpp"
#include "simgrid/s4u.hpp"
#include "control_message.hpp"
#include "heaters_status.hpp"
# include "master.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(smartMeterData, "Messages specific for this s4u example");

SmartMeterData::SmartMeterData(std::string name_) :
    name(name_)
{
    mailbox_controller = simgrid::s4u::Mailbox::by_name(name_ + "_controller");
}

void SmartMeterData::update(double new_load_kw_, HeatersStatus new_heater_status) {
    if (new_load_kw_ >0)
        load_kw = new_load_kw_;
    switch (new_heater_status) {
    case (HeatersStatus::ON):
        if (heaters_status == HeatersStatus::OFF_TO_CONFIRM) {
            XBT_DEBUG("SmartMeterData:update: cannot update status to ON. Waiting to confirm OFF before.");
            return;
        }
        break;
    case (HeatersStatus::OFF):
        if (heaters_status == HeatersStatus::ON_TO_CONFIRM) {
            XBT_DEBUG("SmartMeterData:update: cannot update status to OFF. Waiting to confirm ON before.");
            return;
        }
        else if (heaters_status == HeatersStatus::ON) {
            XBT_DEBUG("SmartMeterData:update: master received shutdown notification without asking for it, ignoring. "
                      "This is a messaging issue between queued data messages and unqueued status messages (data send before status, but received after).");
            return;
        }
        break;
    default:
        xbt_die("Bad update status");
    }

    if (heaters_status == HeatersStatus::OFF_TO_CONFIRM and new_heater_status == HeatersStatus::OFF)
        shutdown_count++;

    heaters_status = new_heater_status;
}
