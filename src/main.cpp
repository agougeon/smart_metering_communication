#include "config.hpp"
#include "district.hpp"
#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include "time.h"
#include "smartmeter.hpp"
#include "phase.hpp"
#include "token.hpp"

#include <fstream>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this s4u example");

static void end_actor(int end_time) {
    simgrid::s4u::this_actor::sleep_until(end_time);
}

std::chrono::_V2::system_clock::time_point start_chrono() {
    return std::chrono::high_resolution_clock::now();
}

void end_chrono(std::chrono::_V2::system_clock::time_point start, std::string logpath) {
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = end-start;
    std::ofstream time_file;
    time_file.open(logpath + "/execution_time.txt");
    time_file << diff.count();
    time_file.close();
}

/// temporary measure of voltage magnitude for anne blavette
//static void voltage_measure(std::string log_dir) {
//    std::vector<std::string> buses = {
//      "bus_ELVTF_619_0_0",  "bus_ELVTF_639_0_0",
//      "bus_ELVTF_886_0_0",  "bus_ELVTF_899_0_0",
//    };

//    std::vector<std::string> phases = {"a", "b", "c"};

//    std::ofstream voltage_file(log_dir + "/voltage_magnitude.csv");
//        voltage_file << "Time (s),Bus,Phase,Voltage Magnitude" << std::endl;

//    simgrid::s4u::this_actor::sleep_until(63000);
//        while (true) {
//            simgrid::s4u::this_actor::sleep_for(1);
//            for (std::string bus : buses)
//                for ( std::string phase : phases)
//                    voltage_file << simgrid::s4u::Engine::get_clock() << ","
//                                 << bus << ","
//                                 << phase << ","
//                                 << simgrid::fmi::get_real("pandapower", "res_bus_3ph/" + bus + "/vm_" + phase + "_pu")
//                                 << std::endl;
//        }
//}

// static void test_sender(int id, int inter_time, std::vector<Phase> sm_phases) {
//     simgrid::s4u::this_actor::sleep_until(inter_time);
//     for (int src = 0; src < 55; src++) {
//         for (int dst = 0; dst < 55; dst++) {
//             if (src == id and src != dst and sm_phases[dst] == sm_phases[id]) {
//                 simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(std::to_string(src) + "-" + std::to_string(dst));
//                 double payload = simgrid::s4u::Engine::get_clock();
//                 mailbox->put(&payload,1000);
//             }
//             simgrid::s4u::this_actor::sleep_until(++inter_time);
//         }
//     }
// }

// static void test_receiver(std::string log_dir, int id, int inter_time, std::vector<Phase> sm_phases) {
//     simgrid::s4u::this_actor::sleep_until(inter_time);
//     for (int src = 0; src < 55; src++) {
//         for (int dst = 0; dst < 55; dst++) {
//            if (dst == id and src != dst and sm_phases[src] == sm_phases[id]) {
//                simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(std::to_string(src) + "-" + std::to_string(dst));
//                double* payload;
//                payload = mailbox->get<double>();
//                std::ofstream config_file(log_dir + "/message_time.csv", std::ios_base::app);
//                config_file << src << "," << dst << "," << simgrid::s4u::Engine::get_clock() - (*payload) << std::endl;
//                config_file.close();
//            }
//            simgrid::s4u::this_actor::sleep_until(++inter_time);
//         }
//     }
// }

// static void simple_sender() {
//     simgrid::s4u::this_actor::sleep_until(10);
//     std::cout << "Sending at: " <<  simgrid::s4u::Engine::get_clock() << std::endl;
//     simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name("test");
//     double payload =  simgrid::s4u::Engine::get_clock();
//     mailbox->put(&payload,1000);
// }

// static void simple_receiver() {
//     simgrid::s4u::this_actor::sleep_until(9);
//     simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name("test");
//     double* payload;
//     payload = mailbox->get<double>();
//     std::cout << "Received at: " << simgrid::s4u::Engine::get_clock() << std::endl;
// }

int main(int argc, char* argv[]) {
    auto start = start_chrono();

    srand(time(NULL));
    simgrid::s4u::Engine e(&argc, argv);
    Config config = Config(argc, argv);
    simgrid::fmi::init(0.1);
    e.load_platform(config.platform_path);
    simgrid::fmi::add_fmu_cs(config.fmu_path, "pandapower", false);
    simgrid::fmi::ready_for_simulation();

    simgrid::s4u::Actor::create("end actor", e.get_all_hosts()[0], end_actor, 67500);
    District district = District(0, config);

    /// measure voltage magnitude (for anne blavette)
//    simgrid::s4u::ActorPtr actor = simgrid::s4u::Actor::create("voltage measure", e.get_all_hosts()[0], voltage_measure, config.log_dir);
//    actor->daemonize();

    /// communication time measurement
//    std::ofstream config_file(config.log_dir + "/message_time.csv");
//    config_file << "source,destination,Time (s)" << std::endl;
//    std::vector<Phase> sm_phases = {
//        Phase::A,Phase::B,Phase::A,Phase::A,Phase::A,Phase::B,Phase::B,Phase::C,Phase::A,Phase::B,
//        Phase::B,Phase::C,Phase::B,Phase::A,Phase::B,Phase::C,Phase::C,Phase::C,Phase::C,Phase::A,
//        Phase::A,Phase::A,Phase::B,Phase::C,Phase::A,Phase::B,Phase::C,Phase::C,Phase::A,Phase::A,
//        Phase::A,Phase::C,Phase::C,Phase::A,Phase::B,Phase::B,Phase::B,Phase::B,Phase::C,Phase::B,
//        Phase::B,Phase::C,Phase::C,Phase::B,Phase::B,Phase::A,Phase::C,Phase::A,Phase::A,Phase::B,
//        Phase::A,Phase::A,Phase::B,Phase::A,Phase::A};

//    for (int i = 0; i < 55; i++) {
//        simgrid::s4u::Actor::create("sender", simgrid::s4u::Host::by_name("smart_meter_" + std::to_string(i)), test_sender, i, 1, sm_phases);
//        simgrid::s4u::Actor::create("receiver", simgrid::s4u::Host::by_name("smart_meter_" + std::to_string(i)), test_receiver, config.log_dir, i, 1, sm_phases);
//    }

    // simgrid::s4u::Actor::create("sender", simgrid::s4u::Host::by_name("smart_meter_0"), simple_sender);
    // simgrid::s4u::Actor::create("receiver", simgrid::s4u::Host::by_name("smart_meter_1"), simple_receiver);


    e.run();

    district.master->log_messages_count();
    end_chrono(start, config.log_dir);
	return 0;
}
