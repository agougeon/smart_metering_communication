//old one-file program created by benjamin camus

#include <iostream>
#include <math.h>
#include <list>
#include <fstream>
#include <sstream>
#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include "argparse.h"
#include <inttypes.h>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this s4u example");

// start time of the simulation
double start_time = 63000;
// duration of the simulation
double simulation_length = 4500;
// sampling period of the simulation output
double continuous_output_step_size = 1;
// type of architecture deployed (0 = no cascado-cyclic, 1 = centralized, 2 = decentralized)
int archi_type;
// are we using unbalanced power network
bool unbalanced;
// maximum cumulative duraton allowed for a heater
double max_cumulative_shutdown_duration = 10 * 60;
// platform path
std::string platform;
// path to log directory
std::string logpath;
/**
* ACTOR PARAMETERS
**/

// current (in kA) in LINE1 above wich the cascado-cyclical process should be started
double ligne1_current_threshold;
// current (in KA) in LINE1 below which the cascado-cyclical process should be stopped (balanced)
double lower_current_threshold = 0.400;
// current (in KA) in LINE1 below which the cascado-cyclical process should be stopped (unbalanceed, one per phase)
double lower_current_threshold_a = 0.400;
double lower_current_threshold_b = 0.400;
double lower_current_threshold_c = 0.400;

// sampling period of the heaters probes
double power_sampling_period;
// sampling period of the LINE1 probe
double current_sampling_period;

// sliding window size for the centralize master to compute the average power consumption of each heater
// (the window spans a period approximately equivalent to avg_sliding_window_size * power_sampling_period
int avg_sliding_window_size = 300;

// size (in bytes) of a message send by the probe of a heater
double sampler_message_size;
// size (in bytes) of a message send from the master to a heater
double control_message_size;
// size of an acknowledgement message send by the heater
double ack_message_size;

// maximum shutdown duration of a heater in the cascado-cyclical process
double max_shutdown_time = 60;

// number of controlable heaters
int nb_heaters;
// number of controlable heaters per phase (unbalanced scenario)
int nb_heaters_a;
int nb_heaters_b;
int nb_heaters_c;
// number of loads
int nb_loads = 55;
// consumption of a heater (in kW)
double heater_consumption = 2;
//number of heaters per household
int nb_heaters_per_household = 3;
// the number of shutdown to perform when the cascado-cyclic process is running (balanced)
int nb_shutdowns;
// the number of shutdown to perform when the cascado-cyclic process is running (unbalanced, one per phase)
int nb_shutdowns_a;
int nb_shutdowns_b;
int nb_shutdowns_c;

/**
* GLOBALS
**/

// the heaters names (which corresponds both the controller name and the FMU input ports ID)
std::vector<std::string> heaters_names;
// the heaters consumptions
std::unordered_map<std::string, std::list<double>> heaters_consumptions;
// the heaters that can be shutdown sorted by priorities order (balanced)
std::vector<std::string> shutdown_priorities;
// the heaters that can be shutdown sorted by priorities order (unbalanced)
std::vector<std::string> shutdown_priorities_a;
std::vector<std::string> shutdown_priorities_b;
std::vector<std::string> shutdown_priorities_c;
// the heaters currently shutdown
std::set<std::string> shutdown_heaters;
// the cascado cyclic manager
simgrid::s4u::ActorPtr cascado_cyclic_actor;
// the timer actor use in the centralized architecture to manage the shutdown time of the heaters
simgrid::s4u::ActorPtr timer_actor;
// the actor used in the centralized architecture to manage the activation and deactivation of cascado-cyclic process
simgrid::s4u::ActorPtr alarm_actor;
// indicate wether the timer_actor is running or not
bool sleeping;
// indicate if the centralized cascado_cyclic manager should start a new iteration or add new heaters in the current iteration
bool new_shutdown;
// indicate if the phases are overloaded, in an unbalanced scenario
std::unordered_map<char, bool> phases_status = {{'a', false}, {'b', false}, {'c', false}};
// indicates if the heaters are shutdown to compute the load of the household
std::vector<double> flexible_loads_status;
// indicates the current flexible consumption of the heaters
std::vector<double> flexible_loads_cons;
// indicates the current non flexible consumption of the heaters
std::vector<double> non_flexible_loads_cons;

// the current in Line1 as known by the master (balanced)
double line1_current;
// the current in Line1 as known by the master (unbalanced, one per phase)
double line1_current_a;
double line1_current_b;
double line1_current_c;

// the number of shutdown cycles completely performed (balanced)
int nb_cycles;
// the number of shutdown cycles completely performed (unbalanced, one per phase)
int nb_cycles_a;
int nb_cycles_b;
int nb_cycles_c;
// used in the decentralized algorithm to be sure that a token from a previous cascado_cyclic process can not be forwarded (balanced)
bool synchronization_needed;
// used in the decentralized algorithm to be sure that a token from a previous cascado_cyclic process can not be forwarded (unbalanced, one per phase)
bool synchronization_needed_a;
bool synchronization_needed_b;
bool synchronization_needed_c;
// indicate wether a decentralized shutdown process is currently running for a heater
std::unordered_map<int, bool> decentralized_shutdown_is_running;
// cumulative shutdown duration per heater (used to be sure that the cumulative duration does not exceed max_cumulative_duration_allowed)
std::unordered_map<std::string, double> cumulative_shutdown_durations;
// times of the last shutdown start for each heater (used to compute the cumulative shutdown duration)
std::unordered_map<std::string, double> last_shutdown_start_time;
// the number of shutdown performed on each heaters
std::unordered_map<std::string, int> heater_nb_shutdowns;
// ID of the mailbox used to send probe data to the controller
std::string master_mailbox_id = "master_mailbox";

/**
* FMU PARAMETERS
**/

// name of the PowerFactory FMU
std::string fmu_name = "pandapower";
// URI of the FMU
std::string fmu_uri;
// step size used by SimGrid to synchronize the FMU
double step_size = 1;

// name of the output port used to retrieve the current in LINE1 (in kA) (balanced)
std::string line1_current_output_id;
// name of the output port used to retrieve the current in LINE1 (in kA) (unbalanced, one per phase)
std::string line1_current_output_id_a;
std::string line1_current_output_id_b;
std::string line1_current_output_id_c;
// prefix and suffix of the output port used to retrieve the power consumption of a house (in W). The port ID equals prefix + house ID + suffix.
std::string P_sum_output_id_prefix = "ElmLod.LOAD";
std::string P_sum_output_id_suffix = ".m:Psum:bus1";
// prefix and suffix of the input port used to activate/deactivate the heater of a house. The port ID equals prefix + house ID + suffix.
std::string controller_input_prefix;
std::vector<std::string> controller_input_suffix;

/*
* OUTPUT
**/

// last time the current crossed the upper threshold
double last_crossing_time;
// cumulative time spend above the upper threshold
double total_peaks_duration;
// max intensity of each peak
double max_peak_intensity;
// status of the heaters (i.e. 1=activated, 0=deactivated)
std::unordered_map<std::string,int> heaters_status;
// number of messages sent
int nb_messages_sent = 0;


// output stream to log the cumulative time spent above the upper threshold
std::ofstream out_total_peaks_duration;
// output stream to log the status of the heaters
std::ofstream out_heaters_status;
// output stream to log the cascado-cyclic process state

/*
 * INPUT
 */

// suffix of flexible load trace files
std::string flexible_load_file_prefix;
// prefix of flexible load trace files
std::string flexible_load_file_suffix = ".csv";
// suffix of non flexible load trace files
std::string non_flexible_load_file_prefix;
// prefix of non flexible load trace files
std::string non_flexible_load_file_suffix = ".csv";

/**
* DATA TYPES
**/

// messages send from the probes to the master
typedef struct{
  std::string name;
  double value;
  double value_a;
  double value_b;
  double value_c;
  
  double time;
} sampler_data;

/**
* UTILITY
**/

static double line1_cur() {
    return simgrid::fmi::get_real(fmu_name, line1_current_output_id);
}
static double line1_cur_a() {
    return simgrid::fmi::get_real(fmu_name, line1_current_output_id_a);
}
static double line1_cur_b() {
    return simgrid::fmi::get_real(fmu_name, line1_current_output_id_b);
}
static double line1_cur_c() {
    return simgrid::fmi::get_real(fmu_name, line1_current_output_id_c);
}

static double time() {
    return simgrid::s4u::Engine::get_clock();
}

// used to detect current peaks in the simulation log
static bool reactOnThresholdCrossing(std::vector<std::string> params){
  if (unbalanced)
    return (params[0]=="above")? (std::max(std::max(line1_cur_a(), line1_cur_b()), line1_cur_c()) >= ligne1_current_threshold) : (std::max(std::max(line1_cur_a(), line1_cur_b()), line1_cur_c()) < ligne1_current_threshold);
  else 
    return (params[0]=="above")? (line1_cur() >= ligne1_current_threshold) : (line1_cur() < ligne1_current_threshold);
}

// used to count the time spend above the threshold in the simulation log
static void recordPeaks(std::vector<std::string> params){
  std::string new_param = "below";

  if(params[0] == "below"){
    total_peaks_duration += time() - last_crossing_time;
    new_param = "above";
  }else{
    max_peak_intensity = 0;
  }

  last_crossing_time = time();
  simgrid::fmi::register_event(reactOnThresholdCrossing, recordPeaks, {new_param});

  out_total_peaks_duration << last_crossing_time << ";" << total_peaks_duration << ";" << max_peak_intensity << "\n";
}

static double computeHeaterAvgCons(std::string heater_name){
  double avg_cons = 0;
  for(double cons : heaters_consumptions[heater_name])
    avg_cons += cons;

  return avg_cons / (double) heaters_consumptions[heater_name].size();
}

// used to sort the heaters in an decreasing consumption order
static bool compareHeaters(std::string h1, std::string h2){
  if(heaters_consumptions.find(h1) == heaters_consumptions.end())
    return false;

  if(heaters_consumptions.find(h2) == heaters_consumptions.end())
    return true;

  return computeHeaterAvgCons(h1) > computeHeaterAvgCons(h2);
}

static void end_of_simulation(){
  int cascado_cyclic_state = (cascado_cyclic_actor == nullptr || cascado_cyclic_actor->is_suspended())? 0 : 1;

  if(archi_type > 0)
    out_heaters_status.close();

  std::string param = (reactOnThresholdCrossing({"above"}))? "below" : "above";
  recordPeaks({param});
  out_total_peaks_duration.close();
}

static void log_controller_output(std::string name, double value){
  heaters_status[name] = value;

  // we compute the cumulative shutdown duration for the heater and check that it does not exceed the maximum allowed
  if(value == 0){
    last_shutdown_start_time[name] = time();
  }else{
    cumulative_shutdown_durations[name] += time() - last_shutdown_start_time[name];

    if(cumulative_shutdown_durations[name] > max_cumulative_shutdown_duration){
      XBT_CRITICAL("HEATER %s EXCEEDS THE MAXIMUM SHUTDOWN DURATION : %f", name.c_str(), cumulative_shutdown_durations[name]);
      std::ofstream error_file;
      error_file.open(logpath + "/" + "ERROR_" + std::to_string(archi_type) + ".txt", std::ios::app);
      error_file << "HEATER " + name + " EXCEEDS THE MAXIMUM SHUTDOWN DURATION : " + std::to_string(cumulative_shutdown_durations[name]) + "\n";
      error_file.close();
    }
  }

  out_heaters_status << simgrid::s4u::Engine::get_clock();
  for(std::string id : heaters_names){
    out_heaters_status << ";" << heaters_status[id];
  }
  out_heaters_status << "\n";
}

static void update_load_consumption(int heater_id){
  double new_consumption = (flexible_loads_cons[heater_id] * flexible_loads_status[heater_id] + non_flexible_loads_cons[heater_id])/1000;
  std::string input_name = controller_input_prefix + std::to_string(heater_id+1) + controller_input_suffix[heater_id+1];
  XBT_DEBUG("load %s has now a consumption of %f mW", input_name.c_str(), new_consumption);
  simgrid::fmi::set_real(fmu_name, input_name, new_consumption);
}

/**
 * ACTORS BEHAVIORS
 */

static void flexible_load_manager(int load_number, int trace_number){
  std::string file_name = flexible_load_file_prefix + std::to_string(trace_number) + flexible_load_file_suffix;
  XBT_DEBUG("Start managing flexible load %i with trace %s", load_number, file_name.c_str());
  double next_time, cons;

  std::ifstream infile(file_name);
  if (!infile.is_open()) {
      XBT_DEBUG("flexible_load_manager: file %s not found.", file_name.c_str());
      exit(0);
  }
  std::string line;
  std::getline(infile, line);

  while(std::getline(infile, line)){

    std::stringstream line_reader(line);
    std::string value;
    std::getline(line_reader,value,';');
    next_time = std::stod(value);
    std::getline(line_reader,value);
    cons = std::stod(value) * 1000;

    if(next_time >= time() && cons!= flexible_loads_cons[load_number]){
      simgrid::s4u::this_actor::sleep_until(next_time);

      XBT_DEBUG("set new value for flexible load %i : %f", load_number, cons);
      flexible_loads_cons[load_number] = cons;
      update_load_consumption(load_number);
    }
  }
  XBT_DEBUG("end of trace for flexible load %i", load_number);
}

static void non_flexible_load_manager(int load_number, int trace_number){

  std::string file_name = non_flexible_load_file_prefix + std::to_string(trace_number+1) + non_flexible_load_file_suffix;
  XBT_DEBUG("Start managing non flexible load %i with trace %s", load_number, file_name.c_str());
  std::ifstream infile(file_name);
  if (!infile.is_open()) {
      XBT_DEBUG("non_flexible_load_manager: file %s not found.", file_name.c_str());
      exit(0);
  }
  std::string line;
  std::getline(infile, line);

  double next_time = 0;

  while(std::getline(infile, line)){
    next_time += 60;
    if(next_time >= time()){
      simgrid::s4u::this_actor::sleep_until(next_time);

      std::stringstream line_reader(line);
      std::string value;
      std::getline(line_reader,value,',');
      std::getline(line_reader,value);

      XBT_DEBUG("set new value for non flexible load %i : %f", load_number, std::stod(value));
      non_flexible_loads_cons[load_number] = std::stod(value);
      update_load_consumption(load_number);
    }
  }
  XBT_DEBUG("end of trace for non flexible load %i", load_number);
}

// Centralized version
static void sampler(int var_id, std::string load_name, double sampling_period){

  sampler_data data;
  data.name = load_name;
  simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(master_mailbox_id);

  XBT_DEBUG("start sampling var %i", var_id);
  while(true){
    simgrid::s4u::this_actor::sleep_for(sampling_period);

    data.time = time();

    if (unbalanced) {
      if(var_id < 0) {
        data.value_a = line1_cur_a();
        data.value_b = line1_cur_b();
        data.value_c = line1_cur_c();
        XBT_DEBUG("sending data { names = %s, %s, %s, value = %f, %f, %f, time = %f}", 
        line1_current_output_id_a.c_str(), line1_current_output_id_b.c_str(), line1_current_output_id_c.c_str(), 
        data.value_a, data.value_b, data.value_c, data.time);
      }
      else {
        data.value = flexible_loads_status[var_id] * flexible_loads_cons[var_id];
        XBT_DEBUG("sending data { name = %s , value = %f, time = %f}", data.name.c_str(), data.value, data.time);
      }
    }
    else {
      if(var_id < 0)
        data.value = line1_cur();
      else
        data.value = flexible_loads_status[var_id] * flexible_loads_cons[var_id];
      XBT_DEBUG("sending data { name = %s , value = %f, time = %f}", data.name.c_str(), data.value, data.time);
    }
    mailbox->put(&data, sampler_message_size);
  }
}

static void controller(int heater_id, std::string load_name){

  simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(load_name);

  XBT_DEBUG("start controlling heater %i", heater_id);
  while(true){
    int command = *((int*) mailbox->get());
    XBT_DEBUG("applying the command : %i", command);
    log_controller_output(load_name, command);
    flexible_loads_status[heater_id] = command;
    update_load_consumption(heater_id);
  }
}

static void timer(){
  sleeping = true;
  simgrid::s4u::this_actor::sleep_for(max_shutdown_time);
  sleeping = false;
}

static void cascado_cyclic_alarm(){
  timer_actor->join();
}

// return the number of shutdown heaters of a specific phase
int shutdown_heaters_in_phase(std::string phase) {
  double count;
  for (std::string heater : shutdown_heaters) {
    //~ XBT_INFO("============== %s ============", heater.c_str());
    //~ XBT_INFO("============== %s ============", heater.substr(5).c_str());
    //~ XBT_INFO("============== %d ============", std::stoi(heater.substr(5)));
    if (controller_input_suffix[std::stoi(heater.substr(5))].find(("_" + phase + "_")) != std::string::npos)
      count++;
  }
  return count;
}

static void cascado_cyclic_manager(){

   int start_shutdown = 0;
   int stop_shutdown = 1;

   nb_cycles = 0;
   nb_cycles_a = 0;
   nb_cycles_b = 0;
   nb_cycles_c = 0;

   sleeping = false;
   new_shutdown = false;

   while(true){
      // check if we start a new iteration or if we simply must add new shutdown in the current iteration
      if(!new_shutdown){
        // send the stop shutdown command
        for(std::string target : shutdown_heaters){
          XBT_DEBUG("sending stop shutdown command to heater %s", target.c_str());
          simgrid::s4u::CommPtr comm = simgrid::s4u::Mailbox::by_name(target)->put_init(&stop_shutdown, control_message_size);
          comm->detach();
        }

        // if we don't have shutdown to perform, then sleep. The master will wake us up when needed.
        if((unbalanced and std::max(std::max(line1_current_a, line1_current_b), line1_current_c) <= ligne1_current_threshold) or 
         (line1_current <= ligne1_current_threshold)){
          XBT_DEBUG("stop cascado-cyclic process because the current in LINE 1 is sufficiently low (%f)", unbalanced ? std::max(std::max(line1_current_a, line1_current_b), line1_current_c) : line1_current);
          simgrid::s4u::this_actor::suspend();
        }

        // we start a timer in a dedicated actor.
        timer_actor = simgrid::s4u::Actor::create("timer", simgrid::s4u::this_actor::get_host(), timer);
        timer_actor->daemonize();

        shutdown_heaters.clear();
      }
      else
        new_shutdown = false;

      // we start an alarm actor that wait for the timer to finish. The master can kill this actor to wake us up sooner.
      alarm_actor = simgrid::s4u::Actor::create("alarm", simgrid::s4u::this_actor::get_host(), cascado_cyclic_alarm);
      alarm_actor->daemonize();

      std::vector<simgrid::s4u::CommPtr> shutdown_comms;

      if (unbalanced) {
        auto it = shutdown_priorities_a.begin();
        while(shutdown_heaters_in_phase("a") < nb_shutdowns_a && shutdown_heaters.size() < nb_heaters) {
          std::string heater = *it;
          // make sure that everybody is shutdown the same amount of times and that a heater can not be selected twice in the same cascado-cyclic iteration
          if(heater_nb_shutdowns[heater] == nb_cycles_a && shutdown_heaters.find(heater) == shutdown_heaters.end()){
            shutdown_heaters.insert(heater);
            heater_nb_shutdowns[heater]++;
            // send the shutdown command
            XBT_DEBUG("sending shutdown command to heater %s", heater.c_str());
            shutdown_comms.push_back(simgrid::s4u::Mailbox::by_name(heater)->put_async(&start_shutdown, control_message_size));
          }
          // if we reach the end of the list, then all the heaters have been selected the same amount of time, and we start a new cycle.
          it++;
          if(it == shutdown_priorities_a.end()){
            it = shutdown_priorities_a.begin();
            nb_cycles_a++;
          }
        }

        it = shutdown_priorities_b.begin();
        while(shutdown_heaters_in_phase("b") < nb_shutdowns_b && shutdown_heaters.size() < nb_heaters) {
           std::string heater = *it;
          // make sure that everybody is shutdown the same amount of times and that a heater can not be selected twice in the same cascado-cyclic iteration
          if(heater_nb_shutdowns[heater] == nb_cycles_b && shutdown_heaters.find(heater) == shutdown_heaters.end()){
            shutdown_heaters.insert(heater);
            heater_nb_shutdowns[heater]++;
            // send the shutdown command
            XBT_DEBUG("sending shutdown command to heater %s", heater.c_str());
            shutdown_comms.push_back(simgrid::s4u::Mailbox::by_name(heater)->put_async(&start_shutdown, control_message_size));
          }
          // if we reach the end of the list, then all the heaters have been selected the same amount of time, and we start a new cycle.
          it++;
          if(it == shutdown_priorities_b.end()){
            it = shutdown_priorities_b.begin();
            nb_cycles_b++;
          }
        }

        it = shutdown_priorities_c.begin();
        while(shutdown_heaters_in_phase("c") < nb_shutdowns_c && shutdown_heaters.size() < nb_heaters) {
          std::string heater = *it;
          // make sure that everybody is shutdown the same amount of times and that a heater can not be selected twice in the same cascado-cyclic iteration
          if(heater_nb_shutdowns[heater] == nb_cycles_c && shutdown_heaters.find(heater) == shutdown_heaters.end()){
            shutdown_heaters.insert(heater);
            heater_nb_shutdowns[heater]++;
            // send the shutdown command
            XBT_DEBUG("sending shutdown command to heater %s", heater.c_str());
            shutdown_comms.push_back(simgrid::s4u::Mailbox::by_name(heater)->put_async(&start_shutdown, control_message_size));
          }
          // if we reach the end of the list, then all the heaters have been selected the same amount of time, and we start a new cycle.
          it++;
          if(it == shutdown_priorities_c.end()){
            it = shutdown_priorities_c.begin();
            nb_cycles_c++;
          }
        }
      }
      else {
        // we search the target according to their priority
        auto it = shutdown_priorities.begin();
        std::vector<simgrid::s4u::CommPtr> shutdown_comms;

        while(shutdown_heaters.size() < nb_shutdowns && shutdown_heaters.size() < nb_heaters ){

          std::string heater = *it;
          // make sure that everybody is shutdown the same amount of times and that a heater can not be selected twice in the same cascado-cyclic iteration
          if(heater_nb_shutdowns[heater] == nb_cycles && shutdown_heaters.find(heater) == shutdown_heaters.end()){
            shutdown_heaters.insert(heater);
            heater_nb_shutdowns[heater]++;
            // send the shutdown command
            XBT_DEBUG("sending shutdown command to heater %s", heater.c_str());
            shutdown_comms.push_back(simgrid::s4u::Mailbox::by_name(heater)->put_async(&start_shutdown, control_message_size));
          }
          // if we reach the end of the list, then all the heaters have been selected the same amount of time, and we start a new cycle.
          it++;
          if(it == shutdown_priorities.end()){
            it = shutdown_priorities.begin();
            nb_cycles++;
          }
        }
      }

      // we wait for all the comms to be over in order to be sure that we do not simutaneously send shutdown and restart command to the heaters
      simgrid::s4u::Comm::wait_all(&shutdown_comms);

      // wait for the timer to finish, or the master to wake us up.
      alarm_actor->join();
   }
}

static void updateNbShutdownAndLowerThreshold(){
  double current_gap, power_gap;
  if (unbalanced) {
    current_gap = line1_current_a - ligne1_current_threshold;
    power_gap = current_gap * std::sqrt(3) * 400;
    nb_shutdowns_a += (int) std::ceil(power_gap / heater_consumption);
    nb_shutdowns_a = std::min(nb_heaters, nb_shutdowns_a);
    lower_current_threshold_a = ligne1_current_threshold - nb_shutdowns_a * heater_consumption * nb_heaters_per_household / (std::sqrt(3) * 400);
    
    current_gap = line1_current_b - ligne1_current_threshold;
    power_gap = current_gap * std::sqrt(3) * 400;
    nb_shutdowns_b += (int) std::ceil(power_gap / heater_consumption);
    nb_shutdowns_b = std::min(nb_heaters, nb_shutdowns_b);
    lower_current_threshold_b = ligne1_current_threshold - nb_shutdowns_b * heater_consumption * nb_heaters_per_household / (std::sqrt(3) * 400);
    
    current_gap = line1_current_c - ligne1_current_threshold;
    power_gap = current_gap * std::sqrt(3) * 400;
    nb_shutdowns_c += (int) std::ceil(power_gap / heater_consumption);
    nb_shutdowns_c = std::min(nb_heaters, nb_shutdowns_c);
    lower_current_threshold_c = ligne1_current_threshold - nb_shutdowns_c * heater_consumption * nb_heaters_per_household / (std::sqrt(3) * 400);
  }
  else {
    // compute the number of heaters to shutdown and the lower threshold
    current_gap = line1_current - ligne1_current_threshold;
    power_gap = current_gap * std::sqrt(3) * 400;
    // we consider the worst case scenario here -i.e. that the household consumption only equals the consumption of a single heater
    nb_shutdowns += (int) std::ceil(power_gap / heater_consumption);
    nb_shutdowns = std::min(nb_heaters, nb_shutdowns);
    // we also consider the worst case scenario to compute the lower threshold -i.e. that the household consumption equals the sum of the consumption of its heaters
    lower_current_threshold = ligne1_current_threshold - nb_shutdowns * heater_consumption * nb_heaters_per_household / (std::sqrt(3) * 400);
  }
}

static void master(){

  simgrid::s4u::Mailbox* mailbox = simgrid::s4u::Mailbox::by_name(master_mailbox_id);

  lower_current_threshold = 99999999999999999;

  nb_shutdowns = 0;
  nb_shutdowns_a = 0;
  nb_shutdowns_b = 0;
  nb_shutdowns_c = 0;

  while(true){
    sampler_data *probe_data = (sampler_data*) mailbox->get();
      XBT_DEBUG("received data from probe %s, value = %f, %f, %f, %f", probe_data->name.c_str(), probe_data->value, probe_data->value_a, probe_data->value_b, probe_data->value_c);

    if(probe_data->name != line1_current_output_id and probe_data->name != line1_current_output_id_a and
       probe_data->name != line1_current_output_id_b and probe_data->name != line1_current_output_id_c){
       heaters_consumptions[probe_data->name].push_front(probe_data->value);
       if(heaters_consumptions[probe_data->name].size() > avg_sliding_window_size)
         heaters_consumptions[probe_data->name].pop_back();
       std::sort(shutdown_priorities.begin(), shutdown_priorities.end(), compareHeaters);
    }else{
      if (unbalanced) {
        line1_current_a = probe_data->value_a;
        line1_current_b = probe_data->value_b;
        line1_current_c = probe_data->value_c;
      }
      else
        line1_current = probe_data->value;
      // check if we need to start new cascado-cyclic shutdowns
      if ((unbalanced and std::max(std::max(line1_current_a, line1_current_b), line1_current_c) >= ligne1_current_threshold) or 
         (line1_current >= ligne1_current_threshold)) {

         updateNbShutdownAndLowerThreshold();

         if(cascado_cyclic_actor->is_suspended()){
          XBT_DEBUG("!!! too much current (%f kA) start cascado-cyclic process !!! ", unbalanced ? std::max(std::max(line1_current_a, line1_current_b), line1_current_c) : line1_current);
          cascado_cyclic_actor->resume();
        }else{
          XBT_DEBUG("!!! too much current (%f kA) but the cascado-cyclic process is already running !!!! ADDING MORE HEATERS !!! ", unbalanced ? std::max(std::max(line1_current_a, line1_current_b), line1_current_c) : line1_current);
          new_shutdown = true;
          alarm_actor->kill();
        }
      }else if (((unbalanced and std::max(std::max(line1_current_a, line1_current_b), line1_current_c) <= ligne1_current_threshold) or 
                (!unbalanced and line1_current <= ligne1_current_threshold)) and sleeping) {
        sleeping = false;
        nb_shutdowns = 0;
        nb_shutdowns_a = 0;
        nb_shutdowns_b = 0;
        nb_shutdowns_c = 0;
        lower_current_threshold = 9999999999999999;
        lower_current_threshold_a = 9999999999999999;
        lower_current_threshold_b = 9999999999999999;
        lower_current_threshold_c = 9999999999999999;
        timer_actor->kill();
      }
    }
    XBT_DEBUG("done processing the data");
  }
}

std::string get_name_first_heater_in_phase(std::string phase) {
  int i = 1;
  std::string first_heater = controller_input_prefix + std::to_string(i) + controller_input_suffix[i];
  while(first_heater.find("_" + phase + "_") == std::string::npos) {
    i++;
    first_heater = controller_input_prefix + std::to_string(i) + controller_input_suffix[i];
  }
  return first_heater;
}

std::string get_name_last_heater_in_phase(std::string phase) {
  int i = nb_heaters + 1;
  std::string last_heater = controller_input_prefix + std::to_string(i) + controller_input_suffix[i];
  while(last_heater.find("_" + phase + "_") == std::string::npos) {
    i--;
    last_heater = controller_input_prefix + std::to_string(i) + controller_input_suffix[i];
  }
  return last_heater;
}

std::string get_name_next_heater_in_phase(std::string phase, int current_heater_id) {
  int i = current_heater_id + 1;
  std::string heater = controller_input_prefix + std::to_string(i) + controller_input_suffix[i];
  while(heater.find("_" + phase + "_") == std::string::npos) {
    i++;
    heater = controller_input_prefix + std::to_string(i) + controller_input_suffix[i];
  }
  return heater;
}


// decentralized version
static void token_generator(int nb_tokens, std::string phase){
  std::string first_heater;
  if (unbalanced)
    first_heater = get_name_first_heater_in_phase(phase);
  else
    first_heater = controller_input_prefix + "1" + controller_input_suffix[1];

  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(first_heater);
  int token_id = nb_cycles;

  XBT_DEBUG("generating the tokens");
  for(int i=0; i < nb_tokens; i++)
    send_mailbox->put(&token_id, control_message_size);
  // to be sure that token_id is received before we leave
  simgrid::s4u::this_actor::yield();
}

static void decentralized_master(){

  int stop_cycle = -1;
  
  simgrid::s4u::Mailbox* mailbox;
  simgrid::s4u::Mailbox* mailbox_a;
  simgrid::s4u::Mailbox* mailbox_b;
  simgrid::s4u::Mailbox* mailbox_c;
  if (unbalanced) {
    mailbox_a = simgrid::s4u::Mailbox::by_name(get_name_first_heater_in_phase("a"));
    mailbox_b = simgrid::s4u::Mailbox::by_name(get_name_first_heater_in_phase("b"));
    mailbox_c = simgrid::s4u::Mailbox::by_name(get_name_first_heater_in_phase("c"));
  }
  else 
    mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + "1" + controller_input_suffix[1]);
  

  bool cascado_cyclic_started = false;
  bool cascado_cyclic_started_a = false;
  bool cascado_cyclic_started_b = false;
  bool cascado_cyclic_started_c = false;
  synchronization_needed = false;
  synchronization_needed_a = false;
  synchronization_needed_b = false;
  synchronization_needed_c = false;

  lower_current_threshold = 9999999999999999;
  lower_current_threshold_a = 9999999999999999;
  lower_current_threshold_b = 9999999999999999;
  lower_current_threshold_c = 9999999999999999;
  nb_shutdowns = 0;
  nb_shutdowns_a = 0;
  nb_shutdowns_b = 0;
  nb_shutdowns_c = 0;

  while(true){

    if (unbalanced) {
      line1_current_a = simgrid::fmi::get_real(fmu_name, line1_current_output_id_a);
      line1_current_b = simgrid::fmi::get_real(fmu_name, line1_current_output_id_b);
      line1_current_c = simgrid::fmi::get_real(fmu_name, line1_current_output_id_c);
    }
    else
      line1_current = simgrid::fmi::get_real(fmu_name, line1_current_output_id);

    int old_nb_shutdowns;
    int old_nb_shutdowns_a;
    int old_nb_shutdowns_b;
    int old_nb_shutdowns_c;
    int nb_tokens;
    if (unbalanced) {
      old_nb_shutdowns_a = nb_shutdowns_a;
      old_nb_shutdowns_b = nb_shutdowns_b;
      old_nb_shutdowns_c = nb_shutdowns_c;
      updateNbShutdownAndLowerThreshold();
      if (line1_current_a >= ligne1_current_threshold) {
        nb_tokens = nb_shutdowns_a;
        if(!cascado_cyclic_started_a){
          XBT_DEBUG("!!! too much current in phase a (%f kA) start cascado-cyclic process !!! ", line1_current_a);
          cascado_cyclic_started_a = true;
        }
        else{
          XBT_DEBUG("!!! too much current in phase a (%f kA) but the cascado-cyclic process is already running !!!! ADDING MORE HEATERS !!! ", line1_current_a);
          nb_tokens -= old_nb_shutdowns_a;
        }
        simgrid::s4u::ActorPtr gen = simgrid::s4u::Actor::create("token_generator", simgrid::s4u::this_actor::get_host(), token_generator, nb_tokens, "a");
        gen->daemonize();
      } else if (line1_current_a <= lower_current_threshold_a && cascado_cyclic_started_a) {
        XBT_DEBUG("stop cascado-cyclic process in phase a because the current in LINE 1 is sufficiently low (%f)", line1_current_a);
        cascado_cyclic_started_a = false;
        // send the stop message to the heaters
        synchronization_needed_a = true;
        nb_shutdowns_a = 0;
        lower_current_threshold_a = 9999999999999999;
        simgrid::s4u::CommPtr comm = mailbox_a->put_init(&stop_cycle, control_message_size);
        comm->detach();
      }

      if (line1_current_b >= ligne1_current_threshold) {
        nb_tokens = nb_shutdowns_b;
        if(!cascado_cyclic_started_b){
          XBT_DEBUG("!!! too much current in phase a (%f kA) start cascado-cyclic process !!! ", line1_current_b);
          cascado_cyclic_started_b = true;
        }
        else{
          XBT_DEBUG("!!! too much current in phase a (%f kA) but the cascado-cyclic process is already running !!!! ADDING MORE HEATERS !!! ", line1_current_b);
          nb_tokens -= old_nb_shutdowns_b;
        }
        simgrid::s4u::ActorPtr gen = simgrid::s4u::Actor::create("token_generator", simgrid::s4u::this_actor::get_host(), token_generator, nb_tokens, "b");
        gen->daemonize();
      } else if (line1_current_b <= lower_current_threshold_b && cascado_cyclic_started_b) {
        XBT_DEBUG("stop cascado-cyclic process in phase a because the current in LINE 1 is sufficiently low (%f)", line1_current_b);
        cascado_cyclic_started_b = false;
        // send the stop message to the heaters
        synchronization_needed_b = true;
        nb_shutdowns_b = 0;
        lower_current_threshold_b = 9999999999999999;
        simgrid::s4u::CommPtr comm = mailbox_b->put_init(&stop_cycle, control_message_size);
        comm->detach();
      }

      if (line1_current_c >= ligne1_current_threshold) {
        nb_tokens = nb_shutdowns_c;
        if(!cascado_cyclic_started_c){
          XBT_DEBUG("!!! too much current in phase a (%f kA) start cascado-cyclic process !!! ", line1_current_c);
          cascado_cyclic_started_c = true;
        }
        else{
          XBT_DEBUG("!!! too much current in phase a (%f kA) but the cascado-cyclic process is already running !!!! ADDING MORE HEATERS !!! ", line1_current_c);
          nb_tokens -= old_nb_shutdowns_c;
        }
        simgrid::s4u::ActorPtr gen = simgrid::s4u::Actor::create("token_generator", simgrid::s4u::this_actor::get_host(), token_generator, nb_tokens, "c");
        gen->daemonize();
      } else if (line1_current_c <= lower_current_threshold_c && cascado_cyclic_started_c) {
        XBT_DEBUG("stop cascado-cyclic process in phase a because the current in LINE 1 is sufficiently low (%f)", line1_current_c);
        cascado_cyclic_started_c = false;
        // send the stop message to the heaters
        synchronization_needed_c = true;
        nb_shutdowns_c = 0;
        lower_current_threshold_c = 9999999999999999;
        simgrid::s4u::CommPtr comm = mailbox_c->put_init(&stop_cycle, control_message_size);
        comm->detach();
      }
    }
    else {
      if(line1_current >= ligne1_current_threshold){
        old_nb_shutdowns = nb_shutdowns;
        updateNbShutdownAndLowerThreshold();
        nb_tokens = nb_shutdowns;
        if(!cascado_cyclic_started){
          XBT_DEBUG("!!! too much current (%f kA) start cascado-cyclic process !!! ", line1_current);
          cascado_cyclic_started = true;
        }else{
          XBT_DEBUG("!!! too much current (%f kA) but the cascado-cyclic process is already running !!!! ADDING MORE HEATERS !!! ", line1_current);
          nb_tokens -= old_nb_shutdowns;
        }
        simgrid::s4u::ActorPtr gen = simgrid::s4u::Actor::create("token_generator", simgrid::s4u::this_actor::get_host(), token_generator, nb_tokens, "");
        gen->daemonize();
      } else if (line1_current <= lower_current_threshold && cascado_cyclic_started) {
        XBT_DEBUG("stop cascado-cyclic process because the current in LINE 1 is sufficiently low (%f)", line1_current);
        cascado_cyclic_started = false;
        // send the stop message to the heaters
        synchronization_needed = true;
        nb_shutdowns = 0;
        lower_current_threshold = 9999999999999999;
        simgrid::s4u::CommPtr comm = mailbox->put_init(&stop_cycle, control_message_size);
        comm->detach();
      }
    }

    simgrid::s4u::this_actor::sleep_for(current_sampling_period);
  }
}

static void decentralized_cycle_manager(){
  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + "1" + controller_input_suffix[1]);
  simgrid::s4u::Mailbox* recv_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + std::to_string(nb_heaters+1) + controller_input_suffix[nb_heaters+1]);

  // manage the cycles
  while(true){
    int token_id = *((int*) recv_mailbox->get());

    if(synchronization_needed){
      synchronization_needed = (token_id >= 0);
    }else{
      // detect new cycle
      if(token_id == nb_cycles)
         nb_cycles++;
      // if we do not receive a stop shutdown command, then forward the new token with the updated token
      if(token_id >= 0)
        send_mailbox->put(&nb_cycles, control_message_size);
    }
  }
}

static void decentralized_cycle_manager_a(){
  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(get_name_first_heater_in_phase("a"));
  simgrid::s4u::Mailbox* recv_mailbox = simgrid::s4u::Mailbox::by_name(get_name_last_heater_in_phase("a"));
  // manage the cycles
  while(true){
    int token_id = *((int*) recv_mailbox->get());
    if(synchronization_needed_a)
      synchronization_needed_a = (token_id >= 0);
    else {
      // detect new cycle
      if(token_id == nb_cycles_a)
         nb_cycles_a++;
      // if we do not receive a stop shutdown command, then forward the new token with the updated token
      if(token_id >= 0)
        send_mailbox->put(&nb_cycles_a, control_message_size);
    }
  }
}

static void decentralized_cycle_manager_b(){
  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(get_name_first_heater_in_phase("b"));
  simgrid::s4u::Mailbox* recv_mailbox = simgrid::s4u::Mailbox::by_name(get_name_last_heater_in_phase("b"));
  // manage the cycles
  while(true){
    int token_id = *((int*) recv_mailbox->get());
    if(synchronization_needed_b)
      synchronization_needed_b = (token_id >= 0);
    else {
      // detect new cycle
      if(token_id == nb_cycles_b)
         nb_cycles_b++;
      // if we do not receive a stop shutdown command, then forward the new token with the updated token
      if(token_id >= 0)
        send_mailbox->put(&nb_cycles_b, control_message_size);
    }
  }
}

static void decentralized_cycle_manager_c(){
  simgrid::s4u::Mailbox* send_mailbox = simgrid::s4u::Mailbox::by_name(get_name_first_heater_in_phase("c"));
  simgrid::s4u::Mailbox* recv_mailbox = simgrid::s4u::Mailbox::by_name(get_name_last_heater_in_phase("c"));
  // manage the cycles
  while(true){
    int token_id = *((int*) recv_mailbox->get());
    if(synchronization_needed_c)
      synchronization_needed_c = (token_id >= 0);
    else {
      // detect new cycle
      if(token_id == nb_cycles_c)
         nb_cycles_c++;
      // if we do not receive a stop shutdown command, then forward the new token with the updated token
      if(token_id >= 0)
        send_mailbox->put(&nb_cycles_c, control_message_size);
    }
  }
}

std::string get_heater_phase(int id) {
  if (controller_input_suffix[id].find("_a_") != std::string::npos)
    return "_a_";
  else if (controller_input_suffix[id].find("_b_") != std::string::npos)
    return "_b_";
  else 
    return "_c_";
}

static void shutdown_behavior(int i, int token_id){

  decentralized_shutdown_is_running[i] = true;

  std::string load_name = "LOAD_"+std::to_string(i);
  simgrid::s4u::Mailbox* send_mailbox;
  if (unbalanced)
    send_mailbox = simgrid::s4u::Mailbox::by_name(get_name_next_heater_in_phase(get_heater_phase(i), i));
  else 
    send_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + std::to_string(i+1) + controller_input_suffix[i+1]);

  XBT_DEBUG("start applying the command : { command = 0, duration = %f }", max_shutdown_time);
  log_controller_output(load_name, 0.0);
  flexible_loads_status[i-1] = 0.0;
  update_load_consumption(i-1);
  simgrid::s4u::this_actor::sleep_for(max_shutdown_time);
  XBT_DEBUG("stop applying the command : { command = 0, duration = %f }", max_shutdown_time);
  log_controller_output(load_name, 1.0);
  flexible_loads_status[i-1] = 1.0;
  update_load_consumption(i-1);

  decentralized_shutdown_is_running[i] = false;

  send_mailbox->put(&token_id, control_message_size);
  // to be sure that the receivers get token_id before we leave
  simgrid::s4u::this_actor::yield();
}

static void decentralized_controller(int id){

  std::string input_name = controller_input_prefix + std::to_string(id) + controller_input_suffix[id];
  std::string phase = get_heater_phase(id);

  simgrid::s4u::Mailbox* recv_mailbox = simgrid::s4u::Mailbox::by_name(input_name);
  simgrid::s4u::Mailbox* send_mailbox;
  if (unbalanced)
    send_mailbox = simgrid::s4u::Mailbox::by_name(get_name_next_heater_in_phase(get_heater_phase(id), id));
  else 
    send_mailbox = simgrid::s4u::Mailbox::by_name(controller_input_prefix + std::to_string(id+1) + controller_input_suffix[id+1]);

  int nb_shutdown = 0;
  simgrid::s4u::ActorPtr shutdown_process;
  decentralized_shutdown_is_running[id] = false;

  int token_id = 0;
  while(true){
    token_id = *((int*) recv_mailbox->get());

    if(token_id == nb_shutdown && !decentralized_shutdown_is_running[id]){
      nb_shutdown++;
      shutdown_process = simgrid::s4u::Actor::create("shutdown_process_"+std::to_string(id), simgrid::s4u::this_actor::get_host(), shutdown_behavior, id, token_id);
      shutdown_process->daemonize();
    }else{
      if(token_id < 0 && decentralized_shutdown_is_running[id]){
        shutdown_process->kill();
        decentralized_shutdown_is_running[id] = false;
    flexible_loads_status[id-1] = 1.0;
        update_load_consumption(id-1);
        log_controller_output("LOAD_"+std::to_string(id), 1);
      }
      send_mailbox->put(&token_id, control_message_size);
    }
  }
}

// global simulation behavior

static void deploy_load_managers(){

  simgrid::s4u::Engine *e = simgrid::s4u::Engine::get_instance();

  for(int i=0;i<nb_loads;i++){
    flexible_loads_status.push_back(1.0);
    flexible_loads_cons.push_back(0.0);
    non_flexible_loads_cons.push_back(0.0);

    simgrid::s4u::ActorPtr current_actor = simgrid::s4u::Actor::create("flexible_load_manager", e->get_all_hosts()[0], flexible_load_manager, i, i);
    current_actor->daemonize();
    current_actor = simgrid::s4u::Actor::create("non_flexible_load_manager", e->get_all_hosts()[0], non_flexible_load_manager, i, i);
    current_actor->daemonize();
  }
}

static void deploy_actors(){

  simgrid::s4u::ActorPtr current_actor = simgrid::s4u::Actor::create("master", simgrid::s4u::Host::by_name("aggregator_1"), master);
  simgrid::s4u::Mailbox::by_name(master_mailbox_id)->set_receiver(current_actor);
  current_actor->daemonize();

  cascado_cyclic_actor = simgrid::s4u::Actor::create("cascado_cyclic_manager", simgrid::s4u::Host::by_name("aggregator_1"), cascado_cyclic_manager);
  cascado_cyclic_actor->daemonize();

  current_actor = simgrid::s4u::Actor::create("sampler_"+line1_current_output_id, simgrid::s4u::Host::by_name("substation_probe_1"), sampler, -1, line1_current_output_id_a, current_sampling_period);
  current_actor->daemonize();

  for(int i=1;i<=nb_heaters;i++){
    std::string sampler_name = P_sum_output_id_prefix + std::to_string(i) + P_sum_output_id_suffix;
    std::string controller_name = controller_input_prefix + std::to_string(i) + controller_input_suffix[i];
    std::string load_name = "LOAD_"+std::to_string(i);

    current_actor = simgrid::s4u::Actor::create("sampler_"+sampler_name, simgrid::s4u::Host::by_name("house_1_" + std::to_string(i) + "_smart_meter"), sampler, i-1, load_name, power_sampling_period);
    current_actor->daemonize();

    current_actor = simgrid::s4u::Actor::create("controller_"+controller_name, simgrid::s4u::Host::by_name("house_1_" + std::to_string(i) + "_smart_meter"), controller, i-1, load_name);
    current_actor->daemonize();

    heaters_names.push_back(load_name);
    if (unbalanced) {
      if (controller_input_suffix[i].find("_a_") != std::string::npos)
        shutdown_priorities_a.push_back(load_name);
      else if (controller_input_suffix[i].find("_b_") != std::string::npos)
        shutdown_priorities_b.push_back(load_name);
      else
        shutdown_priorities_c.push_back(load_name);
    }
    else
      shutdown_priorities.push_back(load_name);
    heaters_status[load_name] = 1.;
    heater_nb_shutdowns[load_name] = 0;
  }
}

static void deploy_decentralized_actors(){

  simgrid::s4u::Engine *e = simgrid::s4u::Engine::get_instance();
  simgrid::s4u::ActorPtr current_actor = simgrid::s4u::Actor::create("decentralized_master", simgrid::s4u::Host::by_name("aggregator_1"), decentralized_master);
  current_actor->daemonize();

  if (unbalanced) {
    cascado_cyclic_actor = simgrid::s4u::Actor::create("decentralized_cyclic_manager", simgrid::s4u::Host::by_name("aggregator_1"), decentralized_cycle_manager_a);
    cascado_cyclic_actor = simgrid::s4u::Actor::create("decentralized_cyclic_manager", simgrid::s4u::Host::by_name("aggregator_1"), decentralized_cycle_manager_b);
    cascado_cyclic_actor = simgrid::s4u::Actor::create("decentralized_cyclic_manager", simgrid::s4u::Host::by_name("aggregator_1"), decentralized_cycle_manager_c);
  }
  else
    cascado_cyclic_actor = simgrid::s4u::Actor::create("decentralized_cyclic_manager", simgrid::s4u::Host::by_name("aggregator_1"), decentralized_cycle_manager);

  cascado_cyclic_actor->daemonize();

  for(int i=1;i<=nb_heaters;i++){
    std::string controller_name = controller_input_prefix + std::to_string(i) + controller_input_suffix[i];
    std::string load_name = "LOAD_"+std::to_string(i);

    current_actor = simgrid::s4u::Actor::create("controller_"+controller_name, simgrid::s4u::Host::by_name("house_1_" + std::to_string(i) + "_smart_meter"), decentralized_controller, i);
    current_actor->daemonize();

    heaters_names.push_back(load_name);
    heaters_status[load_name] = 1.;
    heater_nb_shutdowns[load_name] = 0;
  } 
}

static void initiate_log_folder() {
    system(("mkdir -p " + logpath).c_str());
    std::ofstream config_file;
    config_file.open(logpath + "/config_" + std::to_string(archi_type) + ".csv");
    config_file << "arch_type,nb_heaters,current_threshold,msg_size,sampling_period,platform\n";
    config_file << archi_type << ","
                << nb_heaters << ","
                << ligne1_current_threshold << ","
                << sampler_message_size << ","
                << power_sampling_period << ","
                << platform.substr(platform.find_last_of("/") + 1);
    config_file.close();
}

static void simulation_manager(){
  initiate_log_folder();

  if(archi_type == 1 ){
    out_heaters_status.open(logpath + "/" + "heaters_status.csv", std::ios::out);
    out_total_peaks_duration.open(logpath + "/" + "total_peaks_duration.csv");
  }else if(archi_type == 2){
    out_heaters_status.open(logpath + "/" + "decentralized_heaters_status.csv", std::ios::out);
    out_total_peaks_duration.open(logpath + "/" + "decentralized_total_peaks_duration.csv");
  }else
    out_total_peaks_duration.open(logpath + "/" + "total_peaks_duration_without_cascado_cyclic.csv");

  simgrid::s4u::this_actor::sleep_until(start_time);

  last_crossing_time = start_time;
  std::string param = (reactOnThresholdCrossing({"above"}))? "above" : "below";
  recordPeaks({param});

  XBT_DEBUG("deploying the actors");
  deploy_load_managers();
  if(archi_type > 0){
    if(archi_type == 1){
      deploy_actors();
    }else{
      deploy_decentralized_actors();
    }
  }

  XBT_DEBUG("start observing simulation");
  while(time() <= start_time + simulation_length){
    simgrid::s4u::this_actor::sleep_for(continuous_output_step_size);
    XBT_DEBUG("logging output");
  }

  end_of_simulation();
}

void count_heaters_per_phase() {
  for (std::string suffix : controller_input_suffix) {
    if (suffix.find("_a_") != std::string::npos)
      nb_heaters_a++;
    else if (suffix.find("_b_") != std::string::npos)
      nb_heaters_b++;
    else 
      nb_heaters_c++;
  }
}

void set_command_line_options(int argc, char** argv) {
    Argparse::add_option("-fmu_path", false, "../resources/pandapower_fmu/balanced/pandapower.fmu"); // path to file
    Argparse::add_option("-arch_type", false, "1");
    Argparse::add_option("-nb_heaters", false, "30");
    Argparse::add_option("-current_threshold", false, "0.44");
    Argparse::add_option("-msg_size", false, "10000");
    Argparse::add_option("-sampling_period", false, "10");
    Argparse::add_option("-platform", false, "../resources/platforms/wired_1_district_15ms_latency.xml"); // path to file
    Argparse::add_option("-flexible_loads_path", false, "../resources/input/flexible_loads"); // path to folder
    Argparse::add_option("-non_flexible_loads_path", false, "../resources/input/non_flexible_loads"); // path to folder
    Argparse::add_option("-log_path", false, "../resources/logs"); // path to folder

    Argparse::parse(argc, argv);

    fmu_uri = Argparse::get("-fmu_path");
    archi_type = std::stoi(Argparse::get("-arch_type"));
    nb_heaters = std::stoi(Argparse::get("-nb_heaters"));
    ligne1_current_threshold = std::stod(Argparse::get("-current_threshold"));
    sampler_message_size = std::stod(Argparse::get("-msg_size"));
    control_message_size = sampler_message_size;
    ack_message_size = sampler_message_size;
    power_sampling_period = std::stoi(Argparse::get("-sampling_period"));
    current_sampling_period = power_sampling_period;
    platform = Argparse::get("-platform");
    flexible_load_file_prefix = Argparse::get("-flexible_loads_path") + "/consumption_";
    non_flexible_load_file_prefix = Argparse::get("-non_flexible_loads_path") + "/Load_profile_";
    logpath = Argparse::get("-log_path");
    unbalanced = fmu_uri.find("unbalanced") != std::string::npos;

    if (unbalanced) {
        line1_current_output_id_a = "res_line_3ph/LINE1/i_a_to_ka";
        line1_current_output_id_b = "res_line_3ph/LINE1/i_b_to_ka";
        line1_current_output_id_c = "res_line_3ph/LINE1/i_c_to_ka";
        controller_input_prefix = "asymmetric_load/LOAD";
        controller_input_suffix = {"unused value",
                                   "/p_a_mw","/p_b_mw","/p_a_mw","/p_a_mw","/p_a_mw","/p_b_mw","/p_b_mw","/p_c_mw","/p_a_mw","/p_b_mw",
                                   "/p_b_mw","/p_c_mw","/p_b_mw","/p_a_mw","/p_b_mw","/p_c_mw","/p_c_mw","/p_c_mw","/p_c_mw","/p_a_mw",
                                   "/p_a_mw","/p_a_mw","/p_b_mw","/p_c_mw","/p_a_mw","/p_b_mw","/p_c_mw","/p_c_mw","/p_a_mw","/p_a_mw",
                                   "/p_a_mw","/p_c_mw","/p_c_mw","/p_a_mw","/p_b_mw","/p_b_mw","/p_b_mw","/p_b_mw","/p_c_mw","/p_b_mw",
                                   "/p_b_mw","/p_c_mw","/p_c_mw","/p_b_mw","/p_b_mw","/p_a_mw","/p_c_mw","/p_a_mw","/p_a_mw","/p_b_mw",
                                   "/p_a_mw","/p_a_mw","/p_b_mw","/p_a_mw","/p_a_mw"};

    }
    else {
        line1_current_output_id = "res_line/LINE1/i_to_ka";
        controller_input_prefix = "load/LOAD";
        controller_input_suffix = {"unused value",
                                   "/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw",
                                   "/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw",
                                   "/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw",
                                   "/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw",
                                   "/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw","/p_mw",
                                   "/p_mw","/p_mw","/p_mw","/p_mw","/p_mw"};
    }

}

int main(int argc, char* argv[]) {
    auto start = std::chrono::high_resolution_clock::now();
    simgrid::s4u::Engine e(&argc, argv);
    simgrid::fmi::init(step_size);

    set_command_line_options(argc, argv);

    XBT_INFO("architecture type = %i (0 = no cascado-cyclic, 1 = centralized, 2 = decentralized)", archi_type);
    XBT_INFO("number of households = %i ", nb_heaters);
    XBT_INFO("upper current threshold = %f", ligne1_current_threshold);
    XBT_INFO("message size = %f bytes", sampler_message_size);
    XBT_INFO("sampling period = %f ms", power_sampling_period);
    XBT_INFO("platform path = %s", platform.c_str());
    XBT_INFO("fmu path = %s", fmu_uri.c_str());   

    e.load_platform(platform);

    simgrid::s4u::Actor::create("simulation_manager", e.host_by_name("aggregator_1"), simulation_manager);

    XBT_DEBUG("deploying the FMU");
    simgrid::fmi::add_fmu_cs(fmu_uri, fmu_name, false);
    simgrid::fmi::ready_for_simulation();

    e.run();

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = end-start;
    std::ofstream time_file;
    time_file.open(logpath + "/execution_time_" + std::to_string(archi_type) + ".txt");
    time_file << diff.count();
    time_file.close();
    return 0;
}
