#include "smartmeter.hpp"
#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include "control_message.hpp"
#include "data_message.hpp"
#include "heaters_status.hpp"
#include "management.hpp"
#include "token.hpp"
#include <fstream>
#include <time.h>
#include <math.h>
#include <numeric>

XBT_LOG_NEW_DEFAULT_CATEGORY(smartmeter, "Messages specific for this s4u example");

SmartMeter::SmartMeter(int start_time_s_,
                       int sampling_period_s_,
                       int max_shutdown_duration_s_,
                       int data_message_size_bytes_,
                       bool sheddable_,
                       std::string flexible_loads_dir,
                       std::string non_flexible_loads_dir,
                       std::string fmu_input_,
                       std::string log_dir_,
                       std::string master_name_,
                       simgrid::s4u::Host* host_,
                       Phase phase_,
                       Management management_) :
    start_time_s(start_time_s_),
    sampling_period_s(sampling_period_s_),
    max_shutdown_duration_s(max_shutdown_duration_s_),
    data_message_size_bytes(data_message_size_bytes_),
    sheddable(sheddable_),
    fmu_input_p(fmu_input_),
    log_dir(log_dir_),
    master_name(master_name_),
    host(host_),
    phase(phase_),
    management(management_)
{
    fmu_input_q = fmu_input_p.substr(0, fmu_input_p.size()-2);
    fmu_input_q[fmu_input_q.size() - 4] = 'q';
    fmu_input_q = fmu_input_q + "mvar";

    create_vector_flexible_load(flexible_loads_dir);
    create_vector_non_flexible_load(non_flexible_loads_dir);
    create_mailboxes();
    create_actors();
    XBT_DEBUG("Smart meter %s created on host: %s.", (sheddable ? "controlable" : "non-controlable"), host->get_cname());
};

std::string SmartMeter::get_name() {
    return host->get_name();
}

Phase SmartMeter::get_phase() {
    return phase;
}

bool SmartMeter::is_sheddable() {
    return sheddable;
}

std::string SmartMeter::get_fmu_input_p() {
    return fmu_input_p;
}

void SmartMeter::set_next_mailbox(simgrid::s4u::Mailbox* mailbox) {
    XBT_DEBUG("%s: set_next_mailbox: next mailbox in ring set to \"%s\"", host->get_cname(), mailbox->get_cname());
    mailbox_token_next = mailbox;
}

void SmartMeter::set_next_smartmeter(SmartMeter* next_smartmeter) {
    this->next_smartmeter = next_smartmeter;
}

SmartMeter* SmartMeter::get_next_smartmeter() {
    return next_smartmeter;
}

double SmartMeter::get_average_load_kw() {
    return std::accumulate(sliding_window_loads_kw.begin(), sliding_window_loads_kw.end(), 0.0) / sliding_window_loads_kw.size();
}

void SmartMeter::create_vector_flexible_load(std::string dir_path) {
    std::string file_path = dir_path + "/consumption_" + std::to_string(rand() % 100) + ".csv";
    std::ifstream file(file_path);
    if(!file.is_open())
            throw std::runtime_error("Could not open file: " + file_path);
    int time;
    double value;
    std::string line;
    while (std::getline(file, line)) {
        time = std::stoi(line.substr(0, line.find(";")));
        value = std::stod(line.substr(line.find(";") + 1));
        time_s_flexible_load_kw.push_back(std::pair<int, double>(time,value));
    }
    file.close();
}

void SmartMeter::create_vector_non_flexible_load(std::string dir_path) {
    std::string file_path = dir_path + "/Load_profile_" + std::to_string((rand() % 100) + 1) + ".csv";
    std::ifstream file(file_path);
    if(!file.is_open())
            throw std::runtime_error("Could not open file: " + file_path);
    std::string bad_format_time; // not used, bad format in file
    int time = 0;
    double value = -1;
    double previous_value;
    std::string line;
    std::getline(file, line); // remove header line
    while (std::getline(file, line)) {
        time+= 60;
        if (time >= start_time_s) {
            previous_value = value;
            value = std::stod(line.substr(line.find(",") + 1));
            if (previous_value != value)
                time_s_non_flexible_load_kw.push_back(std::pair<int, double>(time,value));
        }
    }
    file.close();
}

void SmartMeter::update_load() {
    load_kw = heaters_status == HeatersStatus::ON ? flexible_load_kw + non_flexible_load_kw : non_flexible_load_kw;
    double p = load_kw / 1000;
    double s = p / 0.95;
    double q = sqrt(s*s - p*p);
    simgrid::fmi::set_real(fmu_name, fmu_input_p, p);
    simgrid::fmi::set_real(fmu_name, fmu_input_q, q);
}

void SmartMeter::log_shutdown() {
    std::ofstream shutdowns(log_dir + "/shutdowns.csv",  std::ios_base::app);

    shutdowns << host->get_name() << ","
                        << (phase == Phase::A ? "A" : phase == Phase:: B ? "B" : "C") << ","
                        << shutdown_date << ","
                        << simgrid::s4u::Engine::get_clock() - shutdown_date << std::endl;
    shutdowns.close();
}

void SmartMeter::shutdown_timer(SmartMeter* smart_meter) {
    simgrid::s4u::this_actor::sleep_for(smart_meter->max_shutdown_duration_s);
    smart_meter->turn_on_heaters();
}

void SmartMeter::turn_on_heaters() {
    XBT_DEBUG("%s: shutdown_timer: waking up heaters.", host->get_cname());
    /// heaters already ON
    if (heaters_status == HeatersStatus::ON)
        return;

    log_shutdown();
    heaters_status = HeatersStatus::ON;
    update_load();
    if (management == Management::CENTRALIZED)
        send_status_to_master();
}

void SmartMeter::turn_off_heaters() {
    XBT_DEBUG("%s: shutdown_timer: shutting down heaters.", host->get_cname());
    /// heaters already OFF
    if (heaters_status == HeatersStatus::OFF and management == Management::CENTRALIZED) {
        send_status_to_master();
        return;
    }

    shutdown_date = simgrid::s4u::Engine::get_clock();
    heaters_status = HeatersStatus::OFF;
    nb_shutdown++;
    update_load();
    shutdown_timer_actor = simgrid::s4u::Actor::create("shutdown_timer", host, shutdown_timer, this);
    shutdown_timer_actor->daemonize();
    if (management == Management::CENTRALIZED)
        send_status_to_master();
}

void SmartMeter::sliding_window_updater(SmartMeter *smart_meter) {
    simgrid::s4u::this_actor::sleep_until(smart_meter->start_time_s);
    while (true) {
        smart_meter->sliding_window_loads_kw.push_front(smart_meter->load_kw);
        if ((int) smart_meter->sliding_window_loads_kw.size() > smart_meter->sliding_window_size_s)
            smart_meter->sliding_window_loads_kw.pop_back();
        simgrid::s4u::this_actor::sleep_for(1);
    }
}

void SmartMeter::flexible_load_updater(SmartMeter* smart_meter) {
    for (std::pair<int, double> p : smart_meter->time_s_flexible_load_kw) {
        simgrid::s4u::this_actor::sleep_until(p.first);
        smart_meter->flexible_load_kw = p.second;
//        smart_meter->flexible_load_kw = 0; // used to run without any heaters consumption
        smart_meter->update_load();
    }
    throw std::runtime_error("End of file reached - flexible");
}

void SmartMeter::non_flexible_load_updater(SmartMeter* smart_meter) {
    for (std::pair<int, double> p : smart_meter->time_s_non_flexible_load_kw) {
        simgrid::s4u::this_actor::sleep_until(p.first);
        smart_meter->non_flexible_load_kw = p.second;
        smart_meter->update_load();
    }
    throw std::runtime_error("End of file reached - non flexible");
}

void SmartMeter::controller(SmartMeter* smart_meter) {
    simgrid::s4u::this_actor::sleep_until(smart_meter->start_time_s);
    ControlMessage* msg;
    while(true) {
        msg = smart_meter->mailbox_controller->get<ControlMessage>();
        if (*msg == ControlMessage::SHUTDOWN) {
            XBT_DEBUG("%s: controller: shutdown command received.", smart_meter->host->get_cname());
            smart_meter->turn_off_heaters();
        }
        else if (*msg == ControlMessage::WAKEUP) {
            XBT_DEBUG("%s: controller: wakeup command received.", smart_meter->host->get_cname());
            smart_meter->turn_on_heaters();
        }
    }
}

void SmartMeter::controller_decentralized(SmartMeter *smart_meter) {
    Token* token;
    simgrid::s4u::CommPtr com;
    while(true) {
        token = smart_meter->mailbox_token->get<Token>();
        XBT_DEBUG("%s:controller_decentralized: shutdown command received.", smart_meter->host->get_cname());

        switch (smart_meter->heaters_status) {
        case HeatersStatus::ON:
            smart_meter->turn_off_heaters();
            token->houses_to_shutdown--;
            if (token->houses_to_shutdown > 0) {
                com = smart_meter->mailbox_token_next->put_async(new Token(token), smart_meter->control_message_size_bytes);
            }
            else {
                token->next_mailbox = smart_meter->mailbox_token_next->get_name();
                com = smart_meter->mailbox_token_master->put_async(new Token(token), smart_meter->control_message_size_bytes);
            }
            com.detach();
            break;
        case HeatersStatus::OFF:
            token->next_mailbox = smart_meter->get_name() + "_token";
            com = smart_meter->mailbox_token_master->put_async(new Token(token), smart_meter->control_message_size_bytes);
            com.detach();
            break;
        default:
            xbt_die("unreachable");
        }
    }
}

void SmartMeter::send_data_to_master() {
    simgrid::s4u::CommPtr com = mailbox_data_master->put_async(
                new DataMessage(
                    get_average_load_kw(),
                    get_name(),
                    phase,
                    heaters_status),
                data_message_size_bytes);
    com.detach();
}

void SmartMeter::send_status_to_master() {
    mailbox_status_master->put(
                new DataMessage(
                    -1,
                    get_name(),
                    phase,
                    heaters_status),
                status_message_size_bytes);
}

void SmartMeter::sampler(SmartMeter* smart_meter) {
    simgrid::s4u::this_actor::sleep_until(smart_meter->start_time_s);
    while(true) {
        smart_meter->send_data_to_master();
        simgrid::s4u::this_actor::sleep_for(smart_meter->sampling_period_s);
    }
}

void SmartMeter::create_actors() {
    typedef void (func)(SmartMeter*);
    simgrid::s4u::ActorPtr current_actor;

    std::vector<std::pair<std::string, func*>> load_updater_funcs = {
        std::pair<std::string, func*>("flexible_load_updater", &flexible_load_updater),
        std::pair<std::string, func*>("non_flexible_load_updater", &non_flexible_load_updater)};

    std::vector<std::pair<std::string, func*>> control_funcs = {std::pair<std::string, func*>("sliding_window_updater", &sliding_window_updater)};
    if (management == Management::CENTRALIZED) {
        XBT_DEBUG("%s: create_actors: centralized management detected. Creating controller and sampler actor.", host->get_cname());
        control_funcs.push_back(std::pair<std::string, func*>("controller", &controller));
        control_funcs.push_back(std::pair<std::string, func*>("sampler", &sampler));
    }
    else {
        XBT_DEBUG("%s: create_actors: decentralized management detected. Creating decentralized controller actor.", host->get_cname());
        control_funcs.push_back(std::pair<std::string, func*>("controller_decentralized", &controller_decentralized));
    }

    for (auto p : load_updater_funcs) {
        current_actor = simgrid::s4u::Actor::create(p.first, host, p.second, this);
        current_actor->daemonize();
    }

    if (sheddable)
        for (auto p : control_funcs) {
            current_actor = simgrid::s4u::Actor::create(p.first, host, p.second, this);
            current_actor->daemonize();
    }
}

void SmartMeter::create_mailboxes() {
    if (management == Management::CENTRALIZED) {
        mailbox_controller = simgrid::s4u::Mailbox::by_name(get_name() + "_controller");
        mailbox_data_master = simgrid::s4u::Mailbox::by_name(master_name + "_data_smartmeter");
        mailbox_status_master = simgrid::s4u::Mailbox::by_name(master_name + "_status");
    }
    else
        mailbox_token = simgrid::s4u::Mailbox::by_name(get_name() + "_token");
        mailbox_token_master = simgrid::s4u::Mailbox::by_name(master_name + "_token");
}
