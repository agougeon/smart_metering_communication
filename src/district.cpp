#include <bits/stdc++.h>
#include "district.hpp"
#include "simgrid/s4u.hpp"
#include "master.hpp"
#include "substation.hpp"
#include "smartmeter.hpp"


XBT_LOG_NEW_DEFAULT_CATEGORY(district, "Messages specific for this s4u example");

SmartMeter* District::get_next_smartmeter_in_phase(Phase phase, int index) {
    unsigned int i = index;
    while (i < smartmeters.size() &&
          (smartmeters[i]->get_phase() != phase || (smartmeters[i]->get_phase() == phase && !smartmeters[i]->is_sheddable())))
        i++;
    if (i < smartmeters.size())
        return smartmeters[i];
    else
        return get_next_smartmeter_in_phase(phase, 0);
}

void District::set_nexts() {
    SmartMeter* next_smartmeter;
    simgrid::s4u::Mailbox* next_mailbox;
    for (unsigned int i = 0; i < smartmeters.size(); i++) {
        if (smartmeters[i]->is_sheddable()) {
            next_smartmeter = get_next_smartmeter_in_phase(smartmeters[i]->get_phase(),i+1);
            next_mailbox = simgrid::s4u::Mailbox::by_name(next_smartmeter->get_name() + "_token");
            smartmeters[i]->set_next_mailbox(next_mailbox);
            smartmeters[i]->set_next_smartmeter(next_smartmeter);
        }
    }
}

District::District(int id_, Config config) : id(id_) {
    simgrid::s4u::Engine* e = simgrid::s4u::Engine::get_instance();

    master = new Master(id,
                                config.upper_current_threshold_ka,
                                config.log_dir,
                                e->host_by_name("master"),
                                config.management);

    substation = new Substation(start_time,
                                1,
                                config.message_size_bytes,
                                config.log_dir,
                                e->host_by_name("substation"),
                                simgrid::s4u::Mailbox::by_name(master->get_name() + "_data_substation"));

    /// fill a vector with as much true as there is sheddable houses
    std::vector<bool> is_sheddable;
    for (int i = 0; i < config.nb_sheddable_houses; i++)
        is_sheddable.push_back(true);
    for (int i = config.nb_sheddable_houses; i < nb_total_houses; i++)
        is_sheddable.push_back(false);
    std::shuffle(is_sheddable.begin(), is_sheddable.end(), std::default_random_engine(rand()));
    /// instantiate smartmeters
    for (int i = 0; i < nb_total_houses; i++) {
        smartmeters.push_back(new SmartMeter(start_time,
                                             config.sampling_period_s,
                                             config.max_shutdown_duration_s,
                                             config.message_size_bytes,
                                             is_sheddable[i],
                                             config.flexible_loads_dir,
                                             config.non_flexible_loads_dir,
                                             fmu_input[i].first,
                                             config.log_dir,
                                             master->get_name(),
                                             e->host_by_name("smart_meter_" + std::to_string(i)),
                                             fmu_input[i].second,
                                             config.management));
    }
    std::shuffle(smartmeters.begin(), smartmeters.end(), std::default_random_engine(rand()));

    /// if we are in a decentralized scenario the master needs to know
    /// the firsts smartmeter of each ring to sends tokens
    if (config.management == Management::DECENTRALIZED) {
        master->set_first_mailbox_per_ring(get_next_smartmeter_in_phase(Phase::A, 0)->get_name() + "_token",
                                               get_next_smartmeter_in_phase(Phase::B, 0)->get_name() + "_token",
                                               get_next_smartmeter_in_phase(Phase::C, 0)->get_name() + "_token");
        /// assign a next smartmeter to each sheddable house, to create the ring for the decentralized management
        set_nexts();

        for (auto sm : smartmeters) {
            if (sm->is_sheddable()) {
                XBT_DEBUG("src: %s phase: %s sheddable: %d load: %s",
                          sm->get_name().c_str(),
                          (sm->get_phase() == Phase::A? "A": sm->get_phase() == Phase::B? "B" : "C"),
                          sm->is_sheddable(),
                          sm->get_fmu_input_p().c_str());
                XBT_DEBUG("dst: %s phase: %s sheddable: %d load: %s",
                          sm->get_next_smartmeter()->get_name().c_str(),
                          (sm->get_next_smartmeter()->get_phase() == Phase::A? "A": sm->get_next_smartmeter()->get_phase() == Phase::B? "B" : "C"),
                          sm->get_next_smartmeter()->is_sheddable(),
                          sm->get_next_smartmeter()->get_fmu_input_p().c_str());
            }
        }
    }
}
