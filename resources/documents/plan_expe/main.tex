\documentclass{article}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}

\usepackage{xcolor}


\begin{document}

\title{Plan d'Expérimentation}
\author{Adrien Gougeon}

\maketitle

\section{Introduction}

Ce document vise à clarifier les principaux points du projet :
\begin{itemize}
	\item Contexte
	\item Objectif
	\item Cadre Expérimental
	\item Paramètres Mesurés
	\item Paramètre Explorés
	\item Résultats 
\end{itemize}

\section{Contexte}

Ce projet se place dans le cadre d'une grille plus "intelligente" ou smartgrid. Une étape importante pour la transition vers une smartgrid est d'obtenir plus de données des utilisateur afin notamment d'adapter consommation et production au besoin. Cette récolte de données se fait à travers l'utilisation de compteurs intelligents ou smart meters.

Différentes politiques de smart metering ont déjà testés à travers le monde, utilisant des technologies de communications diverses et variées. Malheureusement le choix d'une technologie spécifique est rarement justifié, et les technologies couramment utilisées dans ce cadre n'ont pas été comparés.

\section{Objectif}

Comparer différentes technologies de communication pour effectuer du "demand side management", en s'appuyant sur du smart metering pour faire l'effacement de charge.

\section{Cadre Expérimental}

Afin de comparer les différents technologies nous allons nous appuyer sur des simulations. Nous allons simuler une situation nécessitant d'effectuer du smart metering chez des usagers : de l'effacement de charge. Tous les foyers d'un quartier sont connectés à la même sous-station fournissant de l'électricité. Cette sous-station, et plus précisément son transformateur, peut être surchargée. Afin de palier à cette surcharge, ou congestion, les usagers équipés de radiateurs électriques seront amenés à les éteindre temporairement afin de réduire la charge sur la sous-station. Les radiateurs d'un foyer ne peuvent rester éteint que 60s maximum avant de devoir se rallumer. Ce foyer ne pourra être effacé à nouveau que lorsque tous les foyers reliés à la même phase auront été effacés autant de fois que ce dernier.

Le temps simulé est de 1h15, de 17h30 à 18h15, ce qui correspond au pic de consommation au Royaume-Uni.

\subsection{Architecture}

\subsubsection{Simple}

Dans un premier temps le réseau utilisé sera le réseau décrit dans cet l'article : \cite{9045183} 
ELTVF : https://site.ieee.org/pes-testfeeders/resources/

\subsubsection{Complexe}

Dans un second temps, utiliser le réseau simple dans un cadre plus large en le dupliquant et en rajoutant le réseau de transmission en plus du réseau de distribution. Ce réseau est décrit dans le git "bigger\_elvtf".

\subsection{Consommation des Foyers}

Lors d'une simulation, la consommation de chaque foyers à chaque instant est la somme de sa charge flexible et de sa charge non flexible.

\subsubsection{Charges Non Flexibles}

La charge non flexible d'un foyer correspond à tout sauf ses radiateurs électriques. Nous utilisons des profiles réel de consommation décrit dans cet article : \cite{9045183}. Les mesures sont effectués toute les minutes, et donc mis à jour toute les minutes pendant la simulation. 

Le profile moyen est décrit en Figure \ref{img:load_profiles}. Comme expliqué dans la section "Cadre Expérimental" le temps simulé est entre 17h30 et 18h15, soit entre les 2 barres verticales rouge sur la Figure \ref{img:load_profiles}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/load_profiles.png}
	\caption{Profile moyen des charges flexibles. Les valeurs utilisé pendant la simulation sont celles entre les deux barres verticales rouges, soit entre 17h30 et 18h15.}
	\label{img:load_profiles}
\end{figure}

\subsubsection{Charges Flexibles}

La charge flexible d'une foyers correspond à la consommation de ses radiateurs électriques. Nous considérons ici 3 radiateurs électriques par foyer. La consommation d'un radiateur électrique alterne régulièrement et de façon instantanée entre 0 et sa charge maximale, ici 2kW.

Des profiles de charges flexibles ont été générés aléatoirement en considérant un décalage entre les radiateurs au sein d'un foyer pouvant aller jusqu'à 30 secondes. Les profiles considèrent que les radiateurs peuvent être allumés à partir de 17h30, avec un décalage aléatoire pouvant aller jusqu'à 20 minutes.

L'ajout à posteriori de radiateurs électriques sur le réseau crée un fort déséquilibre entre les phases (>15\%). C'est tout à fait normal étant donné que le réseau initial n'était pas prévu pour supporter une telle charge. L'effacement en tenant compte du déséquilibre entre les phase sera exploré dans de futurs travaux.  

\subsection{Communication}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/smart_metering_wired.png}
	\caption{Représentation simplifiée de la communication entre la sous-station, un foyer et son compteur associé. Communication filaire entre le foyer et le compteur.}
	\label{img:smart_metering_wired}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/smart_metering_wireless.png}
	\caption{Représentation simplifiée de la communication entre la sous-station, un foyer et son compteur associé. Communication sans-fil entre le foyer et le compteur.}
	\label{img:smart_metering_wireless}
\end{figure}

La communication se fait avec une topologie en étoile : chaque compteur communique avec le foyer qui lui est lié, chaque foyer communique avec l'aggregateur de son quartier. L'aggregateur lui communique directement avec la sous-station par un lien filaire.

La communication foyer $\leftrightarrow$ compteur est soit:
\begin{itemize}
	\item filaire : 5ms de latence, 1GBps de débit, représenté en Figure~\ref{img:smart_metering_wired};
	\item sans-fil : wifi 802.11n, mcs 3, nss 1, 54 Mbps de débit max théorique, représenté en Figure~\ref{img:smart_metering_wireless}.
\end{itemize}
	
La communication foyer $\leftrightarrow$ agrégateur se fait théoriquement via internet. En pratique (simulé) la communication entre l'aggregateur et un foyer est représentée par un lien filaire entre les deux entités. La latence de ce lien fait partie des paramètres explorés (voir Section~\ref{param_explored}).

\subsection{Algorithmes d'Effacement}

Pour rester sous le seuil maximum toléré d'intensité il est nécessaire d'effacer certain foyers afin de réduire la charge sur le réseau. Pour cela nous considérons deux algorithmes de gestion : centralisé et décentralisé.

Dans les deux cas de gestion la sous-station envoie toute les secondes à l'agrégateur un relevé de l'intensité de toutes les phases. Si une phase dépasse le seuil maximum toléré alors le mécanisme d'effacement est déclenché par l'agrégateur. Ce mécanisme d'effacement varie selon le mode de gestion.

\subsubsection{Centralisé}

La gestion centralisé est basée sur un contrôle direct de l'agrégateur sur les smartmeter (et donc des foyers qui leurs sont liés). Lorsque le seuil maximum toléré est dépassé sur une phase l'agrégateur détermine combien de foyers il faut effacer en considérant un radiateur électrique actif par foyer. Il envoie ensuite aux foyers ayant la consommation la plus élevée un message de contrôle leur demandant de s'effacer pour une durée d'une minute exactement. L'envoie des messages de contrôle est asynchrone, i.e, il sont tous envoyés en même temps.

La consommation est mesurée localement par chaque smartmeter toute les secondes et la consommation envoyée à l'agrégateur est moyennée sur une fenêtre glissante de cinq minutes. Ces données de consommation ainsi que le statut des foyers (radiateurs éteints ou allumés) sont mises à jour à divers moments :
\begin{itemize}
	\item lorsqu'un smartmeter reçois une demande d'effacement il confirme la commande en envoyant à l'agrégateur un message donnant la consommation du foyer ainsi que son nouveau statut (i.e, éteint);
	\item lorsqu'un smartmeter a terminé son effacement et rallume les radiateurs il envoie sa consommation et son statut (i.e, allumé);
	\item périodiquement, en fonction de la période d'échantillonnage (voir Section~\ref{sec:sampling_period}).
\end{itemize}

Afin de garantir l'équité entre les foyers l'agrégateur compte combien de fois chaque foyers s'est effacé précédemment et maintient aussi un compte du nombre de cycle d'effacement effectué par phase. Un foyer ne peux s'effacer que si le numéro de cycle lié à sa phase est égal au nombre d'effacement de ce foyer. Lorsque tous les foyers ont été effacé le numéro de cycle est incrémenté.

\subsubsection{Décentralisé}

La gestion décentralisé est basée sur l'échange d'un token entre les différents foyers. Initialement, les foyers sont triés en trois anneaux en fonction de la phase à laquelle ils sont connectés, et sont placés autour de ces anneaux dans un ordre aléatoire, fixé au début de la simulation. En accord avec le réseau ELVTF il y a 21 foyers connectés à la phase A, 19 à la phase B et 15 à la phase C.

Lorsque le seuil maximum toléré est dépassé sur une phase l'agrégateur détermine combien de foyers il faut effacer en considérant un radiateur électrique actif par foyer. L'agrégateur envoie au premier foyer de l'anneau concerné un token contenant le nombre de foyers à effacer et le numéro de cycle en cours. À la réception de ce token un foyer peux :
\begin{itemize}
	\item s'effacer (à condition qu'il se soit effacé autant de fois que le numéro de cycle);
	\item marquer dans le token qu'il demande une augmentation de cycle (s'il s'est déjà effacé plus de fois que le numéro de cycle);
	\item passer le token au foyer suivant de l'anneau (s'il s'est effacé et qu'il y a encore des foyers à effacer, s'il ne peux pas s'effacer, ou bien s'il est déjà en cours d'effacement).
\end{itemize}

Si le token revient à l'agrégateur et que le token a été marqué par tous les foyers de l'anneau alors il incrémente le numéro de cycle et renvoie le token au premier foyer de l'anneau, sinon le token est simplement détruit.

Il est à noter que les tokens sont envoyés à la chaîne sans contrôle des tokens déjà en cours de traitement. Cela signifie que si l'agrégateur doit effacer 1 foyer, mais que le token met plus de 1 seconde à atteindre le premier foyer effaçable alors un autre token sera envoyé, effaçant donc à terme 2 foyers au lieux d'un. 

\section{Paramètres Mesurés}

Les paramètres qui seront observés au terme des simulations et qui serviront à comparer l'efficacité des différentes technologies :

\begin{itemize}
	\item Temps passé au dessus du seuil limite (temps de congestion, les mesures étant effectués toutes les seconde on a une précisions à la seconde sur les résultats);
%	\item Temps moyen d'une congestion;
%	\item Temps d'effacement total de tous les foyers;
	\item Temps moyen d'effacement par foyer.
\end{itemize}

\section{Paramètres Explorés}

\label{param_explored}
Les différentes technologies de communication (filaire et wifi) sont comparées sur le même scénario mais avec des paramètres différents en fonction des simulations. Ces paramètres sont regroupés dans la Table \ref{table:explored}. Chaque combinaison est simulé 50 fois selon 2 politiques de gestion : centralisé et décentralisé.

\begin{table}
\begin{tabular}{|c|c|c|c|}
	\hline 
	Paramètre & Limite basse & Limite haute & Pas\\
	\hline 
	Seuil limite pour la sous-station & 0.4 kA & 0.6 kA & $i + O.02$\\
	\hline 
    Nombre de foyers effaçables	& 20 & 55 & $i + 5$\\
	\hline 
	Taille des messages & 1 ko & 100 ko & $10^{i+1}$\\ 
	\hline 
	Période d'échantillonage & 10 s & 1800 s & voir Figure~\ref{img:peaks_vs_sampling_period_(s)}\\
	\hline
	Latence & 10 ms & 100 ms & voir Figure~\ref{img:peaks_vs_latency_(ms)} \\
	\hline
	
\end{tabular} 
\caption{Paramètres Explorés}
\label{table:explored}
\end{table}

\section{Résultats \textcolor{red}{COMMENTAIRES PAS À JOUR}}

Le réseau électriques déséquilibré comprenant 3 phases (A, B et C), les résultats sont présenté sur les figures séparés par phase.

\subsection{Nombre de Foyers Effaçables}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/peaks_(timestep)_vs_sheddable_houses.png}
	\caption{Temps de surcharge par phase en fonction du nombre de maisons effaçables.}
	\label{img:peaks_vs_sheddable_houses}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/shutdown_time_per_house_(s)_vs_sheddable_houses.png}
	\caption{Temps moyen d'effacement par maison et par phase en fonction du nombre de maisons effaçables.}
	\label{img:shutdown_time_per_house_vs_sheddable_houses}
\end{figure}

Le quartier simulé contient dans toutes les simulations 55 foyers. Chaque foyer a une part de charge "flexible" (la consommation de ses 3 radiateurs, chaque radiateur consommant jusqu'à 2kW), et un part de charge "non flexible".
Parmi ces 55 foyers une certain nombre d'entre eux, pris au hasard, seront définis comme effaçable, cela signifie que ces foyers pourront couper temporairement leur radiateurs électriques pour une durée de 60 secondes. Un foyers ne pourra être effacé à nouveau que si tout les autre foyers effaçables sur la même phase ont été effacé autant de fois que ce dernier. Étant donné que les foyers effaçable sont pris au hasard, le nombre de foyers effaçable par phase peut varier d'une simulation à l'autre.

Le temps de surcharge par phase en fonction du nombre de maisons effaçable, du mode de gestion et du type de communication est représenté en Figure~\ref{img:peaks_vs_sheddable_houses}. Il semble que la gestion centralisée est plus à même de gérer les surcharges avec un nombre réduit de foyers effaçables, alors qu'avec une gestion décentralisée le temps de surcharge augmente drastiquement. Il est aussi à noter, hors cas critiques avec peu de foyers effaçables, que le temps de surcharge est légèrement plus faible avec une gestion centralisé et/ou une communication sans fil.

Le temps moyen d'effacement par foyer et par phase en fonction du nombre de maisons effaçable, du mode de gestion et du type de communication est représenté en Figure~\ref{img:shutdown_time_per_house_vs_sheddable_houses}. Il semble que la gestion centralisée profite bien mieux du nombre de foyers effaçable que la gestion décentralisée, qui semble créer beaucoup de sur-effacement. Néanmoins, avec un nombre réduit de foyers effaçable les performances des deux type de gestion semble se rejoindre. Il ne semble pas y avoir d'impact significatif du type de communication sur le temps moyen d'effacement dans ce cas.

\subsection{Seuil de Surcharge}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/peaks_(timestep)_vs_upper_threshold_(ka).png}
	\caption{Temps de surcharge par phase en fonction du seuil de surcharge.}
	\label{img:peaks_vs_upper_threshold_(ka)}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/shutdown_time_per_house_(s)_vs_upper_threshold_(ka).png}
	\caption{Temps moyen d'effacement par maison et par phase en fonction du seuil de surcharge.}
	\label{img:shutdown_time_per_house_vs_upper_threshold_(ka)}
\end{figure}

Le seuil de surcharge définis le seuil maximum de tolérance de consommation d'électricité pour le quartier entier. Ce seuil est définis sur l'intensité et est relevé directement à la sous-station toutes les secondes et transmis à l'agrégateur.

Le temps de surcharge par phase en fonction du seuil de surcharge, du mode de gestion et du type de communication est représenté en Figure~\ref{img:peaks_vs_upper_threshold_(ka)}. Il semble que la gestion décentralisée a bien plus de difficultés à gérer une forte contrainte sur le seuil de surcharge, causant beaucoup plus de dépassements du seuil. Le type de communication ne semble pas avoir d'impact significatif sur le temps de surcharge dans ce cas.

Le temps moyen d'effacement par foyer et par phase en fonction du seuil de surcharge, du mode de gestion et du type de communication est représenté en Figure~\ref{img:shutdown_time_per_house_vs_upper_threshold_(ka)}. Le temps moyen d'effacement des deux modes de gestions semble avoir une comportement similaire, bien que la gestion centralisée cause moins d'effacement que la gestion décentralisée pour chaque seuil. Le type de communication ne semble pas avoir d'impact significatif sur le temps de surcharge dans ce cas.

\subsection{Taille des messages}
\label{sec:message_size}
\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/peaks_(timestep)_vs_message_size_(bytes).png}
	\caption{Temps de surcharge par phase en fonction de la taille des messages.}
	\label{img:peaks_vs_message_size_(bytes)}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/shutdown_time_per_house_(s)_vs_message_size_(bytes).png}
	\caption{Temps moyen d'effacement par maison et par phase en fonction de la taille des messages.}
	\label{img:shutdown_time_per_house_vs_message_size_(bytes)}
\end{figure}

La taille des message définis la taille en bytes de tout message envoyé au sein de notre modèle. Cela comprend les messages :
\begin{itemize}
	\item de contrôle, envoyés de l'agrégateur au smart meters pour demander a un foyers de s'effacer;
	\item de données, envoyés des foyers et de la sous-station vers l'agrégateur et donnant la consommation relevée;
	\item les tokens, créés en cas de gestion décentralisée.
\end{itemize}

Un message de données de la sous-station envoie des mesures instantanées de l'intensité par phase dans la ligne alimentant tout le quartier.
Un message de données d'un smart meter envoie une mesures moyenne de la consommation d'un foyer. La consommation du foyer est relevée toute les secondes et moyennée sur une fenêtre glissante de 5min.

Le temps de surcharge par phase en fonction du nombre de maisons effaçable, du mode de gestion et du type de communication est représenté en Figure~\ref{img:peaks_vs_message_size_(bytes)}. La gestion centralisée semble fortement impactée par des messages de taille supérieure à 100 KB, causant un accroissement conséquent du nombre de dépassements de seuil. À l'inverse, la gestion décentralisée ne semble réellement impactée qu'a partir qu'une taille de 10 MB

Le temps moyen d'effacement par foyer et par phase en fonction du nombre de maisons effaçable, du mode de gestion et du type de communication est représenté en Figure~\ref{img:shutdown_time_per_house_vs_message_size_(bytes)}. L'augmentation de la taille des messages liée à une gestion décentralisée semble accroitre le temps d'effacement moyen. À l'inverse, avec une gestion centralisée il y a une chute du temps d'effacement moyen.

On peux corréler cette chute de l'effacement moyen à la hausse des dépassements de seuils : la période d'échantillonnage de la sous-station étant plus élevée par défaut, l'augmentation de la taille des messages doit surcharger le canal de communication, causant un retard de détection des dépassement de seuil pas l'agrégateur, et donc retardant l'envoie des messages d'effacement aux foyers.

Le type de communication en corrélation avec la taille des message ne semble pas avoir d'impact significatif ni sur les dépassements de seuil ni sur le temps moyen d'effacement.

\subsection{Période d'échantillonnage}
\label{sec:sampling_period}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/peaks_(timestep)_vs_sampling_period_(s).png}
	\caption{Temps de surcharge par phase en fonction de la période d'échantillonage.}
	\label{img:peaks_vs_sampling_period_(s)}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/shutdown_time_per_house_(s)_vs_sampling_period_(s).png}
	\caption{Temps moyen d'effacement par maison et par phase en fonction de la période d'échantillonnage.}
	\label{img:shutdown_time_per_house_vs_sampling_period_(s)}
\end{figure}

La période d'échantillonnage représente le temps entre deux envoies de message de donnée d'un smart meter à l'agrégateur. Comme décrit dans la Section~\ref{sec:message_size}, chaque smart meter relève la consommation du foyer lié à lui toute les secondes et effectue une moyenne de consommation sur une fenêtre glissante de 5min. Cette moyenne est transmise à l'agrégateur à intervalles réguliers, correspondant à la période d'échantillonnage.

Le temps de surcharge par phase en fonction du nombre de maisons effaçable, du mode de gestion et du type de communication est représenté en Figure~\ref{img:peaks_vs_sampling_period_(s)}. La gestion centralisée semble fortement impactée par une période d'échantillonnage supérieure à 120 s, causant un accroissement conséquent du nombre de dépassements de seuil.

Le temps moyen d'effacement par foyer et par phase en fonction du nombre de maisons effaçable, du mode de gestion et du type de communication est représenté en Figure~\ref{img:shutdown_time_per_house_vs_sampling_period_(s)}. L'augmentation de la période d'échantillonnage liée à une gestion centralisée semble diminuer le temps d'effacement moyen.

La gestion décentralisée n'est pas impactée par la période d'échantillonnage car il n'y a aucun échantillonnage fait dans ce mode de gestion.

Le type de communication en corrélation avec la période d'échantillonnage ne semble pas avoir d'impact significatif ni sur les dépassements de seuil ni sur le temps moyen d'effacement.

\subsection{Latence}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/peaks_(timestep)_vs_latency_(ms).png}
	\caption{Temps de surcharge par phase en fonction de la latence.}
	\label{img:peaks_vs_latency_(ms)}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../../images/shutdown_time_per_house_(s)_vs_latency_(ms).png}
	\caption{Temps moyen d'effacement par maison et par phase en fonction de la latence.}
	\label{img:shutdown_time_per_house_vs_latency_(ms)}
\end{figure}

La Latence représente le temps de communication filaire entre un foyer et son smart meter. Cette latence 

Le temps de surcharge par phase en fonction du nombre de maisons effaçable, du mode de gestion et du type de communication est représenté en Figure~\ref{img:peaks_vs_latency_(ms)}. L'augmentation de la latence liée à une gestion centralisée semble fortement augmenter les dépassements de seuils. À l'inverse, la gestion décentraliser semble peu impactée.

Le temps moyen d'effacement par foyer et par phase en fonction du nombre de maisons effaçable, du mode de gestion et du type de communication est représenté en Figure~\ref{img:shutdown_time_per_house_vs_latency_(ms)}. L'augmentation de la latence liée à une gestion centralisée ne semble pas impacter le temps moyen d'effacement. À l'inverse, dans le cas d'une gestion décentralisée, l'augmentation de la latence semble augmenter le temps d'effacement moyen.

Le type de communication en corrélation avec la période d'échantillonnage ne semble pas avoir d'impact significatif ni sur les dépassements de seuil ni sur le temps moyen d'effacement.

\bibliography{references}{}
\bibliographystyle{plain}
\end{document}
