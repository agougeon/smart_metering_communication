#!/usr/bin/env python3

import subprocess
import argparse
import math
import pandas as pd
from tqdm import tqdm
import pandapower as pp
from pandapower.plotting.plotly import pf_res_plotly

# Parsing arguments
parser = argparse.ArgumentParser()
parser.add_argument('-d', '--draw', help="draw generated network", action="store_true")
parser.add_argument('-c', '--csv_path', help="path to directory containing .csv data files\ndefault=\"../documents/European_LV_Test_Feeder_v2/European_LV_CSV_simplified/\"", default="../documents/European_LV_Test_Feeder_v2/European_LV_CSV_simplified/")
parser.add_argument('-nd', '--nb_districts', help="Number of district to build (default=1)", default=1)
parser.add_argument('-ns', '--nb_sub_districts', help="Number of sub district to build (default=1)", default=1)
parser.add_argument('-u', '--unbalanced', help="Build network considering an unbalanced network", action="store_true")
parser.add_argument('-o', '--output', help="Name for the output network (default=\"network.json\")", default="network.json")
args = parser.parse_args()

# Graphic properties for the network representation
h_step = 200
v_step_2 = 200
v_step = v_step_2 * int(args.nb_sub_districts)
# geodata limits from ELVTF
x_min = 390868.753
y_min = 392747.256
# Import ELVTF dataframes
buses = pd.read_csv(args.csv_path + "Buscoords.csv", header=1)
lines = pd.read_csv(args.csv_path +"Lines.csv", header=1)
loads = pd.read_csv(args.csv_path + "Loads.csv", header=2)

if args.unbalanced:
	def create_load_from_row(row):
		P = 0.001
		S = P / 0.95
		Q = math.sqrt(S**2 - P**2)
		bus = pp.get_element_index(net, "bus","bus_ELVTF_" + str(row["Bus"]) + "_" + str(i) + "_" + str(j))
		if row["phases"] == "A":
			pp.create_asymmetric_load(net, bus=bus, p_a_mw=P, q_a_mvar=Q, name=f'{row["Name"]}_{i}_{j}')
		elif row["phases"] == "B":
			pp.create_asymmetric_load(net, bus=bus, p_b_mw=P, q_b_mvar=Q, name=f'{row["Name"]}_{i}_{j}')
		elif row["phases"] == "C":
			pp.create_asymmetric_load(net, bus=bus, p_c_mw=P, q_c_mvar=Q, name=f'{row["Name"]}_{i}_{j}')
		else:
			print("ERROR: unkown phase")
else:
	def create_load_from_row(row):
		P = 2 * 1e-3
		S = P / 0.95
		Q = math.sqrt(S**2- P**2)
		pp.create_load(net, bus=pp.get_element_index(net, "bus","bus_ELVTF_" + str(row["Bus"]) + "_" + str(i) + "_" + str(j)),
			p_mw=P, q_mvar=Q, name=row["Name"])

def create_line_type_from_row(row):
	dic = {
	"r_ohm_per_km": row["R1"],
	"x_ohm_per_km": row["X1"],
	"r0_ohm_per_km": row["R0"],
	"x0_ohm_per_km": row["X0"],
	"c_nf_per_km": row["C1"],
	"c0_nf_per_km": row["C0"],
	"max_i_ka": 1 # arbitrary value
	}
	pp.create_std_type(net, dic, row["Name"], element="line")

net = pp.create_empty_network()
# Lines types
# from ELVTF
pd.read_csv(args.csv_path + "LineCodes.csv", header=1).apply(create_line_type_from_row, axis=1)

# N2YSY 1*50rm 18/30 kV it (rated 30 kV)
# parameters from Anne Blavette
dic = {
"c_nf_per_km": 0.14,
"r_ohm_per_km": 0.3891,
"x_ohm_per_km": 0.1507964,
"c0_nf_per_km": 0.147,
"r0_ohm_per_km": 1.5565,
"x0_ohm_per_km": 0.6031856,
"max_i_ka": 1 # arbitrary value
}
pp.create_std_type(net, dic, "N2YSY 1*50rm 18/30 kV it (rated 30 kV)", element="line")

# N2XSY 1*25 10.00
# parameters from Anne Blavette
dic = {
"c_nf_per_km": 203.0,
"r_ohm_per_km": 0.711,
"x_ohm_per_km": 0.1490001,
"c0_nf_per_km": 203.0,
"r0_ohm_per_km": 1.595,
"x0_ohm_per_km": 0.5550001,
"max_i_ka": 1 # arbitrary value
}
pp.create_std_type(net, dic, "N2XSY 1*25 10.00 kV", element="line")

# 90 MVA 132/33 kV
# parameters from Anne Blavette
dic = {
"sn_mva": 90,
"vn_hv_kv": 132.0,
"vn_lv_kv": 33.0,
"vk_percent": 13.18,
"vkr_percent": 0.27764,
"pfe_kw": 44.9,
"i0_percent": 0.068,
"shift_degree": 30,
"vk0_percent": 10,
"vkr0_percent": 0,
# ~ "vector_group": 'YNd',
"vector_group": 'Dyn',
"mag0_percent": 100,
"mag0_rx": 0,
"si0_hv_partial": 0.9
}
pp.create_std_type(net, dic, "90 MVA 132/33 kV", element="trafo")

# 47 MVA 33/11 kV
# parameters from Anne Blavette
dic = {
"sn_mva": 47,
"vn_hv_kv": 33,
"vn_lv_kv": 11,
"vk_percent": 15,
"vkr_percent": 0.206383,
"pfe_kw": 0,
"i0_percent": 0,
"shift_degree": 0,
"vk0_percent": 11,
"vkr0_percent": 0,
"vector_group": 'YNyn',
"mag0_percent": 100,
"mag0_rx": 0,
"si0_hv_partial": 0.9
}
pp.create_std_type(net, dic, "47 MVA 33/11 kV", element="trafo")

# ELVTF
# from ELVTF
dic = {
"sn_mva": 0.8,
"vn_hv_kv": 11,
"vn_lv_kv": 0.416,
"vk_percent": 4.01995,
"vkr_percent": 0.4,
"pfe_kw": 0,
"i0_percent": 0,
"shift_degree": 180,
"vk0_percent": 6,
"vkr0_percent": 1.30,
"vector_group": 'Yyn',
"mag0_percent": 100,
"mag0_rx": 0,
"si0_hv_partial": 0.9
}
pp.create_std_type(net, dic, "ELVTF", element="trafo")

# Connection to the external grid
pp.create_bus(net, vn_kv=132, name="bus_ext", geodata=(0,0))
pp.create_ext_grid(net,
	bus=pp.get_element_index(net, "bus","bus_ext"), vm_pu=1.05,
	name="Grid_Connection", s_sc_max_mva=2286,
	rx_max=0.09, x0x_max=1.0, r0x0_max=0.09)

if args.unbalanced:
	# Transformer T0
	pp.create_bus(net, vn_kv=33, name="bus_transfo_0", geodata=(2 * h_step,0))
	pp.create_transformer(net,
		hv_bus=pp.get_element_index(net, "bus","bus_ext"),
		lv_bus=pp.get_element_index(net, "bus","bus_transfo_0"),
		std_type="90 MVA 132/33 kV")

else:
	# Impedance of the external grid R0
	pp.create_bus(net, vn_kv=132, name="bus_imp_0", geodata=(h_step,0))
	pp.create_impedance(net,
		from_bus=pp.get_element_index(net, "bus","bus_ext"),
		to_bus=pp.get_element_index(net, "bus","bus_imp_0"),
		rft_pu=0.66/17424, xft_pu=7.57/17424, sn_mva=1000)

	# Transformer T0
	pp.create_bus(net, vn_kv=33, name="bus_transfo_0", geodata=(2 * h_step,0))
	pp.create_transformer(net,
		hv_bus=pp.get_element_index(net, "bus","bus_imp_0"),
		lv_bus=pp.get_element_index(net, "bus","bus_transfo_0"),
		std_type="90 MVA 132/33 kV")

# First Network Division
for i in tqdm(range(0, int(args.nb_districts))):
	# Lines R1
	pp.create_bus(net, vn_kv=33, name=("bus_line_R1_" + str(i)),
		geodata=(3 * h_step, v_step * i))
	pp.create_line(net,
		from_bus=pp.get_element_index(net, "bus","bus_transfo_0"),
		to_bus=pp.get_element_index(net, "bus","bus_line_R1_" + str(i)),
		length_km=5,
		std_type="N2YSY 1*50rm 18/30 kV it (rated 30 kV)",
		name="line_R1_" + str(i))

	#Transformers T1
	pp.create_bus(net, vn_kv=11, name=("bus_transfo_T1_" + str(i)),
		geodata=(4 * h_step, v_step * i))
	pp.create_transformer(net,
		hv_bus=pp.get_element_index(net, "bus","bus_line_R1_" + str(i)),
		lv_bus=pp.get_element_index(net, "bus","bus_transfo_T1_" + str(i)),
		std_type="47 MVA 33/11 kV")

	# Second Network division
	for j in tqdm(range(0, int(args.nb_sub_districts))):
		# Lines R2
		pp.create_bus(net, vn_kv=11, name=("bus_line_R2_" + str(i) + "_" + str(j)),
			geodata=(5 * h_step, v_step * i + v_step_2 * j))
		pp.create_line(net,
			from_bus=pp.get_element_index(net, "bus","bus_transfo_T1_" + str(i)),
			to_bus=pp.get_element_index(net, "bus","bus_line_R2_"  + str(i) + "_" + str(j)),
			length_km=5, std_type="N2XSY 1*25 10.00 kV",
			name="line_R2_"  + str(i) + "_" + str(j))

		# Buses ELVTF
		buses.apply(lambda row:
			pp.create_bus(net, 0.416,
				name="bus_ELVTF_" + str(int(row["Busname"])) + "_" + str(i) + "_" + str(j),
				geodata=(7 * h_step + row[" x"] - x_min, v_step * i + v_step_2 * j + row[" y"] - y_min)), axis=1)

		#Transformers T2
		pp.create_transformer(net,
			hv_bus=pp.get_element_index(net, "bus","bus_line_R2_"  + str(i) + "_" + str(j)),
			lv_bus=pp.get_element_index(net, "bus","bus_ELVTF_1_"  + str(i) + "_" + str(j)),
			std_type="ELVTF")

		# Lines ELVTF
		lines.apply(lambda row:
			pp.create_line(net,
				from_bus=pp.get_element_index(net, "bus","bus_ELVTF_" + str(row["Bus1"]) + "_" + str(i) + "_" + str(j)),
				to_bus=pp.get_element_index(net, "bus","bus_ELVTF_" + str(row["Bus2"]) + "_" + str(i) + "_" + str(j)),
				length_km=row["Length"]/1000.0, std_type=row["LineCode"], name=f'{row["Name"]}_{i}_{j}'), axis=1)

		# Loads ELVTF
		loads.apply(create_load_from_row, axis=1)

if args.unbalanced:
	pp.add_zero_impedance_parameters(net)
	pp.pf.runpp_3ph.runpp_3ph(net)
else:
	pp.runpp(net)

net["converged"] = False
pp.to_json(net, args.output)
if args.draw:
	print("Preparing network to display...")
	pf_res_plotly(net)
