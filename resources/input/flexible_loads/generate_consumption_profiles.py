import numpy as np
import sys
import os

heater_max_timeshift = 30
household_max_timeshift = 20 * 60
nb_heaters_per_household = 3
nb_households = 100
initial_time = 63000

# first, we get the baseline profile
script_dir = os.path.dirname(__file__)
baseline = np.genfromtxt(os.path.join(script_dir, "baseline_profile.csv"), delimiter=';')

for household in range(nb_households):
    # create the output file
    f = open(os.path.join(script_dir, 'consumption_'+str(household)+'.csv'),'w')
    f.write("0;0\n")
    # we randomly generate the timeshift for the household and for the heaters of the household
    household_timeshift = np.random.randint(0, household_max_timeshift)
    heaters_timeshifts = np.random.randint(0, heater_max_timeshift, nb_heaters_per_household)

    # then we take account of the timeshifts to compute the consumption profile of the household
    consumption = -1.0
    for time in range(len(baseline)):
        old_consumption = consumption
        consumption = 0.
        for heater_timeshift in heaters_timeshifts:
            time_shift = household_timeshift + heater_timeshift
            consumption += baseline[max(0, time - time_shift)]

        #write the output line
        timestamp = time + initial_time
        if old_consumption != consumption:
            f.write("%d;%.3f\n" % (timestamp, consumption * 1000))
