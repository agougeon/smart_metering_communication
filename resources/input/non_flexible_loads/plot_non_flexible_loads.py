#!/usr/bin/python3

import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import dates as mdates
import pandas as pd
import datetime as dt

list_df = []
for load_profile in range(1,101):
	tmp_df = pd.read_csv(f'Load_profile_{load_profile}.csv')
	list_df.append(tmp_df[:-1])

df = pd.concat(list_df).reset_index(drop=True)
df.columns = ['Time', 'Power (kW)']
df['Time'] =  pd.to_datetime(df['Time'], format='%H:%M:%S')

sns.set_theme()
sns_plot = sns.lineplot(x='Time', y='Power (kW)', data=df)
sns_plot.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
plt.axvline(dt.datetime(1900,1,1,17,30), c='r')
plt.axvline(dt.datetime(1900,1,1,18,45), c='r')
plt.tight_layout()
plt.savefig("load_profiles.png")
