#!/usr/bin/env python3

# script to create a single platform
# use "create_platforms.py" to create a bunch of platforms


import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-nh', '--nb_houses', type=int, help="number of houses (default=55)", default=55)
parser.add_argument('-w', '--wireless', action='store_true', help="set wireless communication between houses and smartmeters")
parser.add_argument('-dw', '--delay_wan', type=int, help="delay between the edge router and the master in ms (default=10)", default=10)
parser.add_argument('-dn', '--delay_nan', type=float, help="delay between the edge router and the home routers or the substation in ms (default=10)", default =10)
parser.add_argument('-dh', '--delay_han', type=int, help="delay between a home router and a smart meter in ms (default=1)", default =1)
args = parser.parse_args()

nb_houses = args.nb_houses
wireless = args.wireless
d_wan = f'{args.delay_wan}ms'
d_nan = f'{args.delay_nan}ms'
d_han = f'{args.delay_han}ms'

if wireless:
	platform_name = f"wireless_{d_wan}.xml"
else:
	platform_name = f"wired_{d_wan}.xml"

# HEADER
f = open(platform_name, "w")
f.write("""<?xml version='1.0'?><!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<config>
		<prop id="network/model" value="ns-3"/>
		<prop id="ns3/seed" value="time" />
	</config>
	<zone id="world" routing="Floyd">""")

# ZONES
## master
f.write("""
		<zone id="master_zone" routing="Floyd"><host id="master" core="4" speed="1Gf"/></zone>""")
## edge router
f.write("""
		<zone id="edge_router_zone" routing="Floyd"><router id="edge_router"/></zone>""")
## substation
f.write("""
		<zone id="substation_zone" routing="Floyd"><host id="substation" core="4" speed="1Gf"/></zone>""")
## houses
for i in range(nb_houses):
	f.write(f'\n		<zone id="house_{i}_zone" ')
	if wireless:
		f.write(f'routing="WIFI">\
<prop id="access_point" value="home_router_{i}"/>\
<prop id="start_time" value="62990"/>')
	else:
		f.write('routing="Floyd">')
	f.write(f'<router id="home_router_{i}"/>\
<host id="smart_meter_{i}" core="4" speed="1Gf"/>')
	if wireless:
		f.write('</zone>')
	else :
		f.write(f'<link id="home_router_{i}-smart_meter_{i}" bandwidth="1GBps" latency="{d_han}" sharing_policy="SHARED"/>\
<route src="home_router_{i}" dst="smart_meter_{i}">\
<link_ctn id="home_router_{i}-smart_meter_{i}"/></route></zone>')

# ZONE LINKS
f.write("\n")
## master - edge router
f.write(f'\n		<link id="master-edge_router" bandwidth="1GBps" latency="{d_wan}" sharing_policy="SHARED"/>')
## substation - edge router
f.write(f'\n		<link id="substation-edge_router" bandwidth="1GBps" latency="{d_nan}" sharing_policy="SHARED"/>')
## houses - edge router
for i in range(nb_houses):
	f.write(f'\n		<link id="house_{i}-edge_router" bandwidth="1GBps" latency="{d_nan}" sharing_policy="SHARED"/>')

# ZONE ROUTES
f.write("\n")
## master - edge router
f.write(f'\n		<zoneRoute src="master_zone" dst="edge_router_zone"  gw_src="master" gw_dst="edge_router"><link_ctn id="master-edge_router"/></zoneRoute>')
## substation - edge router
f.write(f'\n		<zoneRoute src="substation_zone" dst="edge_router_zone"  gw_src="substation" gw_dst="edge_router"><link_ctn id="substation-edge_router"/></zoneRoute>')
## houses - edge router
for i in range(nb_houses):
	f.write(f'\n		<zoneRoute src="house_{i}_zone" dst="edge_router_zone" gw_src="home_router_{i}" gw_dst="edge_router"><link_ctn id="house_{i}-edge_router"/></zoneRoute>')

# FOOTER
f.write("""
	</zone>
</platform>""")
f.close()
