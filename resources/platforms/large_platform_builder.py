#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-nd', '--nb_districts', type=int, help="number of districts in the network (default=1)", default=1)
parser.add_argument('-ns', '--nb_sub_districts', type=int, help="number of sub district in the network (default=1)", default=1)
parser.add_argument('-nh', '--nb_houses_per_district', type=int, help="number of houses per district (default=55)", default=55)
parser.add_argument('-w', '--wireless', action='store_true', help="set wireless communication between houses and smartmeters")
parser.add_argument('-il', '--internet_latency', type=int, help="latency between distant entities such as aggregator and houses (default=10)", default=10)
parser.add_argument('-ll', '--local_latency', type=int, help="latency between local entities such as aggregator and substation (default=5)", default =5)
args = parser.parse_args()

nb_districts = args.nb_districts
nb_sub_districts = args.nb_sub_districts
nb_houses_per_district = args.nb_houses_per_district
wireless = args.wireless
internet_latency = f'{args.internet_latency}ms'
local_latency = f'{args.local_latency}ms'

if wireless:
	platform_name = f"wireless_{nb_districts}_dis_{nb_sub_districts}_sub_{internet_latency}_latency.xml"
else:
	platform_name = f"wired_{nb_districts}_dis_{nb_sub_districts}_sub_{internet_latency}_latency.xml"

print(platform_name)
	
# HEADER
f = open(platform_name, "w")
f.write("""<?xml version='1.0'?><!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<config>
		<prop id="network/model" value="ns-3"/>
		<prop id="ns3/seed" value="time" />
	</config>
	<zone id="world" routing="Floyd">""")

# ZONES
## source
f.write("""
		<zone id="source_zone" routing="Floyd"><host id="source" core="4" speed="1Gf"/></zone>""")
		
## substations
### main
f.write(f'\n\
		<zone id="main_substation_zone" routing="Floyd">\
<host id="main_substation" core="4" speed="1Gf"/>\
<host id="main_aggregator" core="4" speed="1Gf"/>\
<link id="smain_substation-main_aggregator" bandwidth="1GBps" latency="{local_latency}" sharing_policy="SHARED"/>\
<route src="main_substation" dst="main_aggregator">\
<link_ctn id="smain_substation-main_aggregator"/></route>\
</zone>')

### secondary
for i in range(nb_districts):
	f.write(f'\n\
		<zone id="secondary_substation_{i}_zone" routing="Floyd">\
<host id="secondary_substation_{i}" core="4" speed="1Gf"/>\
<host id="secondary_aggregator_{i}" core="4" speed="1Gf"/>\
<link id="secondary_substation_{i}-secondary_aggregator_{i}" bandwidth="1GBps" latency="{local_latency}" sharing_policy="SHARED"/>\
<route src="secondary_substation_{i}" dst="secondary_aggregator_{i}">\
<link_ctn id="secondary_substation_{i}-secondary_aggregator_{i}"/></route>\
</zone>')

### ternary
for i in range(nb_districts):
	for j in range(nb_sub_districts):
		f.write(f'\n\
		<zone id="ternary_substation_{i}_{j}_zone" routing="Floyd">\
<host id="ternary_substation_{i}_{j}" core="4" speed="1Gf"/>\
<host id="ternary_aggregator_{i}_{j}" core="4" speed="1Gf"/>\
<link id="ternary_substation_{i}_{j}-ternary_aggregator_{i}_{j}" bandwidth="1GBps" latency="{local_latency}" sharing_policy="SHARED"/>\
<route src="ternary_substation_{i}_{j}" dst="ternary_aggregator_{i}_{j}">\
<link_ctn id="ternary_substation_{i}_{j}-ternary_aggregator_{i}_{j}"/></route>\
</zone>')

## houses
for i in range(nb_districts):
	for j in range(nb_sub_districts):
		for k in range(nb_houses_per_district):
			f.write(f'\n		<zone id="house_{i}_{j}_{k}_zone" ')
			if wireless:
				f.write(f'routing="WIFI">\
<prop id="access_point" value="router_{i}_{j}_{k}"/>\
<prop id="start_time" value="62990"/>')
			else:
				f.write('routing="Floyd">')
			f.write(f'<router id="router_{i}_{j}_{k}"/>\
<host id="smart_meter_{i}_{j}_{k}" core="4" speed="1Gf"/>')
			if wireless:
				f.write('</zone>')
			else :
				f.write(f'<link id="router_{i}_{j}_{k}-smart_meter_{i}_{j}_{k}" bandwidth="1GBps" latency="{local_latency}" sharing_policy="SHARED"/>\
<route src="router_{i}_{j}_{k}" dst="smart_meter_{i}_{j}_{k}">\
<link_ctn id="router_{i}_{j}_{k}-smart_meter_{i}_{j}_{k}"/></route></zone>')


# ZONE LINKS
f.write("\n")
## source <-> main aggregator
f.write(f'\n		<link id="source-main_aggregator" bandwidth="1GBps" latency="{internet_latency}" sharing_policy="SHARED"/>')

## main aggregator <-> secondary aggregators
for i in range(nb_districts):
	f.write(f'\n		<link id="main_aggregator-secondary_aggregator_{i}" bandwidth="1GBps" latency="{internet_latency}" sharing_policy="SHARED"/>')
	
## secondary aggregators <-> ternary aggregators
for i in range(nb_districts):
	for j in range(nb_sub_districts):
		f.write(f'\n		<link id="secondary_aggregator_{i}-ternary_aggregator{i}_{j}" bandwidth="1GBps" latency="{internet_latency}" sharing_policy="SHARED"/>')
		
## ternary aggregators <-> house routers
for i in range(nb_districts):
	for j in range(nb_sub_districts):
		for k in range(nb_houses_per_district):
			f.write(f'\n		<link id="ternary_aggregator_{i}_{j}-router_{i}_{j}_{k}" bandwidth="1GBps" latency="{internet_latency}" sharing_policy="SHARED"/>')

# ZONE ROUTES
f.write("\n")
## source <-> main aggregator
f.write(f'\n		<zoneRoute src="source_zone" dst="main_substation_zone"  gw_src="source" gw_dst="main_aggregator"><link_ctn id="source-main_aggregator"/></zoneRoute>')

## main aggregator <-> secondary aggregators
for i in range(nb_districts):
	f.write(f'\n		<zoneRoute src="main_substation_zone" dst="secondary_substation_{i}_zone"  gw_src="main_aggregator" gw_dst="secondary_aggregator_{i}"><link_ctn id="main_aggregator-secondary_aggregator_{i}"/></zoneRoute>')

## secondary aggregators <-> ternary aggregators
for i in range(nb_districts):
	for j in range(nb_sub_districts):
		f.write(f'\n		<zoneRoute src="secondary_substation_{i}_zone" dst="ternary_substation_{i}_{j}_zone" gw_src="secondary_aggregator_{i}" gw_dst="ternary_aggregator{i}_{j}"><link_ctn id="secondary_aggregator_{i}-ternary_aggregator{i}_{j}"/></zoneRoute>')

### ternary aggregators <-> houses
for i in range(nb_districts):
	for j in range(nb_sub_districts):
		for k in range(nb_houses_per_district):
			f.write(f'\n		<zoneRoute src="ternary_substation_{i}_{j}_zone" dst="house_{i}_{j}_{k}_zone" gw_src="ternary_aggregator_{i}_{j}" gw_dst="router_{i}_{j}_{k}"><link_ctn id="ternary_aggregator_{i}_{j}-router_{i}_{j}_{k}"/></zoneRoute>')

# FOOTER 
f.write("""
	</zone>
</platform>""")
f.close()
