#!/usr/bin/env python3

# script to create a bunch of platforms

import os
for d_wan in [0,1,5,10,20,50,100,150]:
    os.system(f'./platform_builder.py -dw {d_wan}')
    os.system(f'./platform_builder.py -dw {d_wan} -w')
