# Generate a plot of the current in the main substation during the simulation

# Should be used inside a log folder

import pandas as pd
import seaborn as sns
from matplotlib import dates as mdates

df_substation_sampler = pd.read_csv('substation_sampler.csv')
upper_threshold_ka = pd.read_csv('config.csv').at[0,'Upper Threshold (kA)']
df_substation_sampler['Peaks (timestep)'] = df_substation_sampler['Current (kA)'].apply(lambda v: v > upper_threshold_ka)
df_substation_sampler = df_substation_sampler.drop(['Time (s)', 'Current (kA)'], axis=1)
df_peaks = df_substation_sampler.groupby(['Phase'], as_index=False).sum()
print(int(df_peaks.loc[df_peaks['Phase'] == 'A', 'Peaks (timestep)']))

df = pd.read_csv("substation_sampler.csv")
df = df.rename({'Time (s)': 'Time'}, axis=1)

df['Hours'] = pd.to_datetime(df['Time'], unit='s')

sns.set_theme()
sns.set_context('talk')
sns.set(rc={'figure.figsize':(7,4)})
ax = sns.lineplot(x="Hours", y="Current (kA)", hue="Phase", data=df)
df_config = pd.read_csv("config.csv")
ax.axhline(df_config.iloc[0]["Upper Threshold (kA)"], color="red", ls="--")
ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
ax.set_xticks([
    pd.to_datetime(63000, unit='s'), 
    pd.to_datetime(63900, unit='s'), 
    pd.to_datetime(64800, unit='s'),
    pd.to_datetime(65700, unit='s'),
    pd.to_datetime(66600, unit='s'),
    pd.to_datetime(67500, unit='s')])

ax.get_figure().tight_layout()
ax.get_figure().savefig("substation_current.pdf")
