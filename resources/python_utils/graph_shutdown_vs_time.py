# Generate a plot of the number of shutdown houses over time

# Should be used inside a log folder

import pandas as pd
import seaborn as sns
from matplotlib import dates as mdates

df = pd.read_csv('shutdowns.csv')

shutdown_dict={
'Date (s)': [],
'Phase': [],
}

for index, row in df.iterrows():
	start = int(row['Date (s)'])
	duration = int(row['Duration (s)'])
	phase = row['Phase']
	for i in range(start, start + duration):
		shutdown_dict['Date (s)'].append(i)
		shutdown_dict['Phase'].append(phase)

df_shutdowns = pd.DataFrame(shutdown_dict)
df_shutdowns = df_shutdowns.groupby(['Date (s)', 'Phase'], as_index=False).size()
df_shutdowns.columns = ['Time', 'Phase', 'Count']
df_shutdowns['Time'] =  pd.to_datetime(df_shutdowns['Time'], unit='s')

sns.set_theme()
sns.set(rc={'figure.figsize':(20,10)})
sns_plot = sns.lineplot(x="Time", y="Count", hue="Phase", data=df_shutdowns)
sns_plot.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
figure = sns_plot.get_figure()
figure.savefig("shutdowns_count.png")
