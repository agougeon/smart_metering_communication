# NOTE: anne blavette request, files produced by the simulation and used
# to build plots might be removed from the main program

# Generate a plot of the voltage magnitude of several buses

# Should be used inside a log folder

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_theme()

df = pd.read_csv("voltage_magnitude.csv")
row_list = []
for i in range(0, len(df), 3):
	if df.at[i,"Bus"] != df.at[i + 1,"Bus"] or 	df.at[i,"Bus"] != df.at[i + 2,"Bus"]:
		print("ERROR: les 3 valeurs ne sont pas du même bus")
	values  = [df.at[i,"Voltage Magnitude"], df.at[i + 1,"Voltage Magnitude"], df.at[i + 2,"Voltage Magnitude"]]
	row_list.append({"Time (s)": df.at[i,"Time (s)"], "Bus":  df.at[i,"Bus"],"Max Voltage Delta (%)": (max(values) - min(values)) / max(values) * 100})

df_delta = pd.DataFrame(row_list, columns=['Time (s)', 'Bus', 'Max Voltage Delta (%)'])

for bus in df_delta:		
	sns_plot = sns.lineplot(x="Time (s)", y="Max Voltage Delta (%)", hue="Bus", data=df_delta)
	plt.tight_layout()
	plt.savefig('max_voltage_delta.png')
	plt.close()
