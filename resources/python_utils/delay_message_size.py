#!/usr/bin/python3
# run simulation and store values from the flow monitor into a csv file
# require the ns3 monitor to be active!

import subprocess
import xml.etree.ElementTree as ET
import pandas as pd

df_delay = {'Comm.': [],
			'Latency (ms)': [],
			'Message Size (bytes)': [],
			'Average Delay (ms)': []}

for platform_path in ['../resources/platforms/wired_1_district_10ms_latency.xml',
				 '../resources/platforms/wireless_1_district_10ms_latency.xml']:
	comm = 'wired' if 'wired' in platform_path else 'wireless'
	latency = int(platform_path.split('_')[3][:-2])
	for message_size in ['1000', '10000', '100000']:
		message_size_int = int(message_size)
		for replica in range(50):
			subprocess.run(f'./smart_metering_communication {message_size} -platform_path {platform_path}', shell=True, check=True)
			tree = ET.parse('Flow-Monitor_PLC.xml')
			root = tree.getroot()
			delay_sum = float(root[0][0].attrib['delaySum'][1:-2]) / 1e6
			tx_packets = int(root[0][0].attrib['txPackets'])
			average_delay = delay_sum / tx_packets
			df_delay['Comm.'].append(comm)
			df_delay['Latency (ms)'].append(latency)
			df_delay['Message Size (bytes)'].append(message_size_int)
			df_delay['Average Delay (ms)'].append(average_delay)

df_delay = pd.DataFrame(df_delay)
df_delay.to_csv('average_delay.csv', index=False)
			
			
			
