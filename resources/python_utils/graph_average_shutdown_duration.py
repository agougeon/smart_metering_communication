# Generate a plot of the average shutdown duration per sheddable houses grouped by phase

# Should be used inside a log folder

import pandas as pd
import seaborn as sns

df = pd.read_csv('shutdowns.csv')
df_total = df.groupby(['Name'],as_index=False).sum()

df = df.drop(['Duration (s)'], axis=1)
df = df.drop_duplicates(keep='first').set_index('Name')
df_total = df_total.set_index('Name')
df_total = df_total.drop(columns=['Date (s)'])
df_total = df_total.join(df)
df_total = df_total.sort_values(by=['Phase'])

sns.set_theme()
sns_plot = sns.barplot(x='Phase', y='Duration (s)', data=df_total)
figure = sns_plot.get_figure()
figure.tight_layout()
figure.savefig("shutdowns_per_phase.png")
