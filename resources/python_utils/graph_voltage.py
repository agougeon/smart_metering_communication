# NOTE: anne blavette request, files produced by the simulation and used
# to build plots might be removed from the main program

# Generate a plot of the voltage magnitude of several buses

# Should be used inside a log folder

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_theme()

df = pd.read_csv("voltage_magnitude.csv")

for bus in df["Bus"].unique():
	# ~ for phase in df["Phase"].unique() :		
	sns_plot = sns.lineplot(x="Time (s)", y="Voltage Magnitude", hue="Phase", data=df[(df["Bus"] == bus)])
	plt.tight_layout()
	plt.savefig(f"{bus}_voltage_magnitude.png")
	plt.close()
