#!/usr/bin/python3

# Generate plots showing the impact of several parameters on simulations
# duration

# NOTE: you must indicate the path to the folder containing all the 
# folders containing the simulations logs

import os
import pandas as pd
import seaborn as sns
from tqdm import tqdm

logs_path = 'logs'

not_finished = 0
simulations_not_finished = []
# from a simulation create a dataframe with 1 row
# columns are simulation parameters and results
def simulation_to_dataframe(folder_path):
		df = pd.read_csv(f'{folder_path}/config.csv')
		with open(f'{folder_path}/execution_time.txt') as f:
			df['Execution Time (m)'] = float(f.readline()) / 60
		return df

# create an iterable of the simulations
def load_simulations():
	for folder in tqdm(os.listdir(logs_path)):
		if (os.path.exists(f'{logs_path}/{folder}/config.csv') and
			os.path.exists(f'{logs_path}/{folder}/execution_time.txt')):
			yield simulation_to_dataframe(f'{logs_path}/{folder}')
		else:
			global not_finished
			global simulations_not_finished
			not_finished += 1
			simulations_not_finished.append(pd.read_csv(f'{logs_path}/{folder}/config.csv'))

# draw plots
sns.set_theme()
print("Loading simulations data")
df = pd.concat(load_simulations(), ignore_index=True)
print("Building graphs")
for col in df.columns:
	if col != 'Execution Time (m)':
		sns_plot = sns.boxplot(data=df, x=col, y="Execution Time (m)", showfliers=True)
		if col == "Platform":
			sns_plot.tick_params(axis='x', labelrotation=90)
		sns_plot.set(ylim=(0))
		figure = sns_plot.get_figure()
		figure.tight_layout()
		figure.savefig(f'execution_time_vs_{col}.png')
		figure.clear()
try:
	df_simulations_not_finished = pd.concat(simulations_not_finished)
	for col in df_simulations_not_finished.columns:
		print(df_simulations_not_finished.groupby([col]).size().to_string())
except ValueError:
	pass

print(f'Unfinished simulations: {not_finished}')
