import pandas as pd
import seaborn as sns

df = pd.DataFrame({})

for comm in ['wired', 'wireless', 'plc']:
    for msg_size in ['1kB', '10kB', '100kB']:
        if comm == 'plc' and not msg_size == '1kB':
            continue
        tmp = pd.read_csv(f'message_time_{msg_size}_{comm}.csv')
        tmp['Message size'] = msg_size
        tmp['Comm.'] = comm
        tmp['Time (ms)'] = tmp['Time (s)'] * 1000
        df = pd.concat([df, tmp], ignore_index=True)

df = df[(df['Comm.'] == 'plc') & (df['Message size'] == '1kB')]
print(df['Time (ms)'].mean())
print(df['Time (ms)'].std())


# sns.set_theme(font_scale=1.2)
# sns_plot = sns.boxplot(x='Message size', y='Time (ms)', hue='Comm.', data=df, showfliers = False)
# sns_plot.figure.tight_layout()
# sns_plot.figure.savefig("message_time")
