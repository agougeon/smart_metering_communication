#!/usr/bin/python3

import execo_engine
import time
import subprocess
import os
import json

########################################################################
                       # GLOBAL PARAMETERS #
########################################################################

# Time interval between two batches (in minutes)
time_interval = 1

# Walltime (h:m:s)
walltime = "24:0:0"

# Size of a job batch (grid5000 accept 200)
nb_concurrent_jobs = 200

########################################################################

# return the number of jobs currently running (on grid5000)
def running_jobs():
	cmd = subprocess.run("oarstat -u -J", capture_output=True, text=True, shell=True)
	try:
		jobs_running = len(json.loads(cmd.stdout))
	except json.decoder.JSONDecodeError:
		jobs_running = 0
	return jobs_running

# Open err and out files to log master behaviour
out = open("master.out", "a+", buffering=1)
err = open("master.err", "a+", buffering=1)

# Generate sweeps
base_nb_sheddable_houses = "30"
base_upper_current_threshold_ka = "0.50"
base_message_size_bytes = str(10 ** 3)
base_max_shutdown_duration_s = "60"
base_sampling_period_s = "20"
base_platforms = ["../resources/platforms/wired_10ms.xml",
				  "../resources/platforms/wireless_10ms.xml"]

sweeps = execo_engine.sweep({
"replica": [str(i) for i in range(50)],
"management": [1,2],
"variable": {
	"none": {
		"nb_sheddable_houses": [base_nb_sheddable_houses],
		"upper_current_threshold_ka": [base_upper_current_threshold_ka],
		"message_size_bytes": [base_message_size_bytes],
		"max_shutdown_duration_s": [base_max_shutdown_duration_s],
		"sampling_period_s": [base_sampling_period_s],
		"platform_path": base_platforms,
		},
	"nb_sheddable_houses": {
		"nb_sheddable_houses": [str(i) for i in range(15,60,5) if str(i) != base_nb_sheddable_houses],
		"upper_current_threshold_ka": [base_upper_current_threshold_ka],
		"message_size_bytes": [base_message_size_bytes],
		"max_shutdown_duration_s": [base_max_shutdown_duration_s],
		"sampling_period_s": [base_sampling_period_s],
		"platform_path": base_platforms,
		},
	"upper_current_threshold_ka": {
		"nb_sheddable_houses": [base_nb_sheddable_houses],
		"upper_current_threshold_ka": [str(i/100.0) for i in range(40,62,2) if str(i/100.0) != base_upper_current_threshold_ka],
		"message_size_bytes": [base_message_size_bytes],
		"max_shutdown_duration_s": [base_max_shutdown_duration_s],
		"sampling_period_s": [base_sampling_period_s],
		"platform_path": base_platforms,
		},
	"message_size_bytes": {
		"nb_sheddable_houses": [base_nb_sheddable_houses],
		"upper_current_threshold_ka": [base_upper_current_threshold_ka],
		"message_size_bytes": [1e4, 1e5], # in bytes (octets)
		"max_shutdown_duration_s": [base_max_shutdown_duration_s],
		"sampling_period_s": [base_sampling_period_s],
		"platform_path": base_platforms,
		},
	"max_shutdown_duration_s": {
		"nb_sheddable_houses": [base_nb_sheddable_houses],
		"upper_current_threshold_ka": [base_upper_current_threshold_ka],
		"message_size_bytes": [base_message_size_bytes],
		"max_shutdown_duration_s": [10, 20, 30, 120, 300],
		"sampling_period_s": [base_sampling_period_s],
		"platform_path": base_platforms,
		},
	"sampling_period_s": {
		"nb_sheddable_houses": [base_nb_sheddable_houses],
		"upper_current_threshold_ka": [base_upper_current_threshold_ka],
		"message_size_bytes": [base_message_size_bytes],
		"max_shutdown_duration_s": [base_max_shutdown_duration_s],
		"sampling_period_s": [15, 30, 60, 120, 300, 600, 900, 1800],
		"platform_path": base_platforms,
		},
	"platform_path": {
		"nb_sheddable_houses": [base_nb_sheddable_houses],
		"upper_current_threshold_ka": [base_upper_current_threshold_ka],
		"message_size_bytes": [base_message_size_bytes],
		"max_shutdown_duration_s": [base_max_shutdown_duration_s],
		"sampling_period_s": [base_sampling_period_s],
		"platform_path": ["../resources/platforms/wired_0ms.xml",
						  "../resources/platforms/wired_1ms.xml",
		                  "../resources/platforms/wired_5ms.xml",
		                  "../resources/platforms/wired_20ms.xml",
		                  "../resources/platforms/wired_50ms.xml",
		                  "../resources/platforms/wired_100ms.xml",
		                  "../resources/platforms/wired_150ms.xml",
						  "../resources/platforms/wireless_0ms.xml",
						  "../resources/platforms/wireless_1ms.xml",
						  "../resources/platforms/wireless_5ms.xml",
						  "../resources/platforms/wireless_20ms.xml",
						  "../resources/platforms/wireless_50ms.xml",
						  "../resources/platforms/wireless_100ms.xml",
						  "../resources/platforms/wireless_150ms.xml"],
		}
	}
})

sweeper = execo_engine.ParamSweeper("sweeps", sweeps=sweeps, save_sweeps=True)

# Print an error and exit if environment file is not present
if not os.path.exists("env.sif") :
	out.write("Environment \"env.sif\" not found.\n")
	out.write("Before using master you must build the environment by connecting \
to a node and then run : \
\"sudo-g5k /grid5000/code/bin/singularity build --sandbox env.sif env.def\"\n")
	exit(0)

out.write("----------------\n" + \
		  "| sweeps state |\n" + \
		  "----------------\n" + \
		  "in progress: " + str(len(sweeper.get_inprogress())) + "\n" + \
		  "done: " + str(len(sweeper.get_done())) + "\n" + \
		  "remaining: " + str(len(sweeper.get_remaining())) + "\n" + \
		  "jobs running: " + str(running_jobs()) + "\n")

# Launch jobs from remaining sweeps
for j in range(min(nb_concurrent_jobs - running_jobs(), len(sweeper.get_remaining()))):
	out.write("Starting job " + str(j) + "\n")
	subprocess.run("oarsub -t besteffort -l core=1,walltime=" + walltime +
				   " \"/grid5000/code/bin/singularity exec env.sif ./slave.py\"",
				   shell=True, stdout=out, stderr=err)

# Go to sleep
out.write("Going to sleep for " + str(time_interval) + " minutes.\n")
time.sleep(time_interval * 60)

# Cancel zombie sweeps (still in progress but no job running it)
if running_jobs() == 0:
	out.write("No jobs running. Canceling zombie sweeps in progress.\n")
	subprocess.run("rm sweeps/inprogress", shell=True, stdout=out, stderr=err)

# Spawn a new master if there is still work to do
if running_jobs() > 0 or len(sweeper.get_inprogress()) > 0 or len(sweeper.get_remaining()) > 0:
	out.write("Spawning a new master to reload sweeps files.\n")
	subprocess.run("./master.py &", shell=True, stdout=out, stderr=err)
else:
	out.write("Nothing more to do. Exiting.")
