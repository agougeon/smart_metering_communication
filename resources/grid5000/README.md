# WARNING
 Building the environment with singularity seems broken on grid5000. Until solved build it locally with the following command:
```
$ sudo singularity build env.sif env.def
```
Then in the next section replace the command in step 1 by the following line and skip step 2:
```
$ scp env.sif master.py slave.py check_sweeps.py <g5k_login>@access.grid5000.fr:rennes/

```


# Running Simulations
---

1. Export environment definition, master, slave and check scripts to the grid5000 frontend

    ```
    $ scp env.def master.py slave.py check_sweeps.py <g5k_login>@access.grid5000.fr:rennes/ 
    ```

2. Build environment (on a grid5000 job)

    ```
    $ sudo-g5k /grid5000/code/bin/singularity build env.sif env.def
    ```

3. Run the simulations (on the frontend)

    ```
    $ ./master.py &
    ```

4. (optionnal) Check jobs still running or finished (on the frontend)

    ```
    $ ./check_sweeps.py
    ```


5. Compress logs after the end of the simulations (on the frontend)

    ```
    $ zip -r logs.zip logs
    ```

6. Retrieve logs (after a while)

    ```
    $ scp <g5k_login>@access.grid5000.fr:rennes/logs.zip .
    ```

## Something went wrong

If something went wrong and you want to retry running the simulations you have to use the following command:


```
$ ./check_sweeps -c
```

It will delete jobs still running and delete folder and files from the previous simulations to avoid corruption of the next one.
Be careful that all jobs have been killed before launching new one (can be checked with the "oarstat" command on grid5000 terminal)


# Generate graphs
---
In folder resources/python_utils you will find several python scripts to generate graphs from one or several simulations logs.

## Graphs for a single simulation
Those files should be placed directly inside the log folder of one simulation, next to files "config.csv" and others.

* graph\_substation\_current.py: plot the current measured at the substation during the simulation
* graph\_average_shutdown_duration.py: plot the average shutdown duration per house, grouped by phase
* graph_shutdown_vs_time.py: plot the number of houses being shutdown during the simulation

## Graphs for several simulation
Those files should be placed next to the folder containing all the logs folder of the simulations.

* graphs\_logs.py: generate plots showing the impact of the explored parameters on the number of mesures with an overload, and on the average shedding time per house
* graphs\_execution\_time.py: generate plots showing how the parameters impact the execution time of the simulations

