#!/usr/bin/python3

import execo_engine
import subprocess

# Retrieve sweeps
sweeper = execo_engine.ParamSweeper("sweeps")

# Launch a job
next_sweep = sweeper.get_next()
if next_sweep != None:
	cmd = "cd /opt/smart_metering_communication/bin && ./smart_metering_communication"
	log_dir_option = " -log_dir $HOME/logs/"
	for k,v in next_sweep.items():
		if k != "variable":
			if k != "replica":
				cmd += f" -{k} {v}"
			if k == "platform_path":
				log_dir_option += f"{v.split('/')[3]}_"
			else:
				log_dir_option += f"{v}_"

	cmd = cmd + log_dir_option[:-1]
	subprocess.run(cmd,shell=True, check=True)
	sweeper.done(next_sweep)
