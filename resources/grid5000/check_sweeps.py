#!/usr/bin/python3

import execo_engine
import subprocess
import argparse
import json
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--clean", help="clean the environment: kill running jobs and delete traces of the previous simulations to avoid corruption",
					action="store_true")
parser.add_argument("-i", "--inprogress", help="show sweeps in progress",
					action="store_true")
parser.add_argument("-r", "--remaining", help="show sweeps remaining",
					action="store_true")
args = parser.parse_args()

def running_jobs():
	cmd = subprocess.run("oarstat -u -J", capture_output=True, text=True, shell=True)
	try:
		jobs_running = len(json.loads(cmd.stdout))
	except json.decoder.JSONDecodeError:
		jobs_running = 0
	return jobs_running
	
try:
	sweeper = execo_engine.ParamSweeper("sweeps")
	inprogress = len(sweeper.get_inprogress())
	done = len(sweeper.get_done())
	remaining = len(sweeper.get_remaining())
	running = running_jobs()
except:
	inprogress = 0
	done = 0
	remaining = 0
	running = 0

if not args.clean:
	print("sweeps \"In progress\": ", inprogress)
	print("sweeps \"Done\": ", done)
	print("sweeps \"Remaining\":", remaining)
	print("jobs running:", running)	
	if inprogress == 0 and remaining == 0 and running == 0:
		answer = input("Simulations finished. Compress logs? [Y/n]")
		if answer.lower() not in ["n","no","non"]:
			subprocess.run("zip -r logs.zip logs", shell=True)
else:
	cmd = subprocess.run("oarstat -u -J", capture_output=True, text=True, shell=True)
	try:
		jobs_running = json.loads(cmd.stdout)
		cmd = "oardel"
		for job in jobs_running:
			cmd += " " + job
		subprocess.run(cmd, shell=True)
	except json.decoder.JSONDecodeError:
		pass
	cmd = subprocess.run("killall master.py", capture_output=True, text=True, shell=True)
	cmd = subprocess.run("rm -rf master.out master.err logs logs.zip sweeps OAR*", capture_output=True, text=True, shell=True)

if args.inprogress:
	print("Details about sweeps in progress:")
	d = defaultdict(lambda:defaultdict(int))
	for sweep in sweeper.get_inprogress():
		for k,v in sweep.items():
			d[k][v] += 1
	for k,v in d.items():
		print(k,v)
if args.remaining:
	print("Details about remaining sweeps:")
	d = defaultdict(defaultdict(int))
	for sweep in sweeper.get_inprogress():
		for k,v in sweep.items():
			d[k][v] += 1
	for k,v in d.items():
		print(k,v)
